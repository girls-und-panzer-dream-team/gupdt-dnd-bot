import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";

export const RacingHelpCommand = new Command()
  .match(matchPrefixes("help"))
  .use(async (context) => {
    const { manager, message } = context;

    let targetChannel = message.channel as TextChannel
    message.channel.send(`Please check the quickguide in the following Google Documentation:\nhttps://tinyurl.com/yyjfvt3e`)
  });

  //https://tinyurl.com/yyjfvt3e
