import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { RacingMain } from "../racing-main"

export const RacingJoinCommand = new Command()
  .match(matchPrefixes("join"))
  .use(async (context) => {
    const { manager, message } = context;

    let targetChannel = message.channel as TextChannel
    if (targetChannel.topic.includes("[rp-track]")) {
      const service = manager.getService(RacingMain);
      service.joinRace(message);
    }
  });
