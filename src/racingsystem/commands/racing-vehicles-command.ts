import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { tankNames } from "../../battlesystem/tanklist.json";
import { carNames, carStats } from "../racing-cars.json";

export const RacingVehiclesCommand = new Command()
  .match(matchPrefixes("vehicles"))
  .use(async (context) => {
    const { manager, message } = context;

    message.channel.send("**Tanks:**\n" +
        "Light Tanks: `" + tankNames.nameListLight.toString() + "`\n" +
        "Medium Tanks: `" + tankNames.nameListMedium.toString() + "`\n" +
        "Heavy Tanks: `" + tankNames.nameListHeavy.toString() + "`\n" +
        "Tank Destroyers: `" + tankNames.nameListTankDestroyer.toString() + "`\n" + 
        "Super Heavies: `" + tankNames.nameListSuperHeavy.toString() + "`\n" +
        "MBTs: `" + tankNames.nameListMBT.toString() + "`");
    
    let vehicleMessage = "**Cars:**\n";

    carNames.forEach(car => {
        vehicleMessage = vehicleMessage + "- " + car + " - SPD: `" + carStats[car].hitThreshold + "`\n"; 
    })

    message.channel.send(vehicleMessage)    
});

