import { Service } from "@enitoni/gears-discordjs";
import { RacingRecord } from "./model/racing-playrecord"
import { Message, GuildMember, Role } from "discord.js";
import { GuPDTRacetrackDao } from "../database/dao/gupdt-racetrack-dao";
import { GuPDTRacetrack } from "../database/entity/gupdt-racetrack";
import { tankNames, tankStats } from "../battlesystem/tanklist.json";
import { carNames, carStats } from "./racing-cars.json";
import { GuPDTRacer } from "../database/entity/gupdt-racer";


export class RacingMain extends Service {
    private raceRecords: RacingRecord[] = [];

    public async createTrack(message: Message) {
        if (this.isAdmin(message.member)) {
            const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
            let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);

            if (track == null) {
                let cleanString = message.content.substring(20).replace(/\s+/g, " ").trim();
                let args = cleanString.split(" ");
                track = new GuPDTRacetrack();
                track.trackName = args[0];
                track.channel = message.channel.id;
                track.guild = message.guild.id;
                track.racers = [];
                track.trackActive = false;
                track.eventActive = false;
                track.trackLength = Number.parseInt(args[1]);
                track.trackCorners = args[2];
                
                let resultTrack = await gupdtRaceTrackDao.updateTrack(track);
                message.channel.send(`(New Track: \`${resultTrack.trackName}\` with length: \`${resultTrack.trackLength}\` and corners: \`${resultTrack.trackCorners}\` created)`)
            }
        }
        return;
    }

    public async toggleTrack(message: Message) {
        if (this.isAdmin(message.member)) {
            const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
            let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
            if (track != null) {
                track.trackActive = !track.trackActive;
                let resultTrack = await gupdtRaceTrackDao.updateTrack(track);
                message.channel.send(`(Track: \`${resultTrack.trackName}\` is now \`${track.trackActive ? "active" : "inactive"}\`)`)
            }
        }
        return;
    }

    public async toggleEvent(message: Message) {
        if (this.isAdmin(message.member)) {
            const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
            let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
            if (track != null) {
                track.eventActive = !track.eventActive;
                track.racers = [];
                let resultTrack = await gupdtRaceTrackDao.updateTrack(track);
                message.channel.send(`(Track: \`${resultTrack.trackName}\` is now ${track.eventActive ? "an event track" : "no longer an event track"})`)
            }
        }
        return;
    }

    public async removeTrack(message: Message) {
        if (this.isAdmin(message.member)) {
            const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
            let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
            if (track != null) {
                await gupdtRaceTrackDao.removeTrack(track);
                message.channel.send(`(Track: \`${track.trackName}\` has been removed)`)
            }
        }
        return;
    }

    public async joinRace(message: Message) {
        const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
        let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
        if (track != null) {
            if (track.trackActive) {
                if (this.isTrackFull(track)) {
                    message.channel.send(`(There is already 4 People racing please wait a bit to avoid too much clutter)`)
                    return;
                }
                if (!this.hasAttemptsLeft(track, message)) {
                    message.channel.send(`(While the track is in Event mode you may only try to race 3 times, ${message.member.displayName}!)`)
                    return;
                }
                if (this.findRaceRecord(message.channel.id, message.member.id, message.guild.id) == null) {
                    let tankString = message.content.substring(12);

                    let tankList: string[] = [];
                    tankList = tankList.concat(tankNames.nameListMedium);
                    tankList = tankList.concat(tankNames.nameListLight);
                    tankList = tankList.concat(tankNames.nameListHeavy);
                    tankList = tankList.concat(tankNames.nameListTankDestroyer);
                    tankList = tankList.concat(tankNames.nameListSuperHeavy);
                    tankList = tankList.concat(tankNames.nameListMBT);
                    let carList: string[] = carNames;

                    let found = false;
                    let car = false;
                    for(let i = 0; i < tankList.length; i++) {
                        let regex = new RegExp (tankList[i], "i")
                        if (regex.test(tankString)) {
                            found = true;
                            tankString = tankList[i];
                            break;
                        }
                    }
                    if (!found) {
                        for(let i = 0; i < carList.length; i++) {
                            let regex = new RegExp(carList[i], "i")
                            if (regex.test(tankString)) {
                                found = true;
                                car = true;
                                tankString = carList[i];
                                break;
                            }
                        }
                    }
                    if (!found) {
                        message.channel.send(`(Please choose a Vehicle from the list! \`!racing vehicles\`)`)
                        return;
                    }

                    let newRecord = new RacingRecord();
                    newRecord.channel = message.channel.id;
                    newRecord.currentStep = 0;
                    newRecord.guild = message.guild.id;
                    newRecord.lap = 1;
                    newRecord.time = 0;
                    newRecord.user = message.member.id;
                    newRecord.userName = message.member.displayName;
                    newRecord.speed = car ? carStats[tankString].hitThreshold : tankStats[tankString].hitThreshold;
                    newRecord.vehicle = tankString;
                    this.raceRecords.push(newRecord);
                    message.channel.send(`(You can now start running the track, ${message.member.displayName}!)\n(Use \`!racing drive [speed]\` speed: \`slow; normal; fast\`)`)
                } else {
                    message.channel.send(`(You are already running the track, ${message.member.displayName}!)\n(Use \`!racing drive [speed]\` speed: \`slow; normal; fast\`)`)
                }
            } else {
                message.channel.send(`(The track is currently not active, ${message.member.displayName}!)`)
            }
        } else {
            message.channel.send(`(No track is setup for this channel, ${message.member.displayName}!)`)
        }
        return;
    }
    
    private hasAttemptsLeft(track: GuPDTRacetrack, message: Message): boolean {
        if (track.eventActive) {
            let racer: GuPDTRacer;
            for (let i = 0; i < track.racers.length; i++) {
                if (track.racers[i].racer === message.member.id) {
                    return track.racers[i].attempts < 3;
                }
            }
        }
        return true;
    }

    public async displayLeaderBoard(message: Message) {
        const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
        let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
        if (track != null) {
            track.racers.sort((a: GuPDTRacer, b: GuPDTRacer) => {
                if (a.bestTime < b.bestTime) {
                    return -1;
                }
                if (a.bestTime > b.bestTime) {
                    return 1;
                } 
                if (a.bestTime === b.bestTime) {
                    if (a.attempts < b.attempts) {
                        return -1;
                    }
                    if (a.attempts > b.attempts) {
                        return 1;
                    }
                }
                return 0;
            })
            let participantString = ``;
            for (let i = 0; i < track.racers.length; i++) {
                let guildMember = await message.guild.members.fetch(track.racers[i].racer);
                participantString = participantString + `#${i + 1} - ${guildMember.displayName} - Time: ${track.racers[i].bestTime} - Attempts: ${track.racers[i].attempts}\n` 
            }
            message.channel.send(`***RACE LEADERBOARD FOR ${track.trackName.toUpperCase()}***\n\`\`\`${participantString}\`\`\``)
        } else {
            message.channel.send(`(No track is setup for this channel, ${message.member.displayName}!)`)
        }
        return;
    }

    private isTrackFull(track: GuPDTRacetrack): boolean {
        let numRacers = 0;
        this.raceRecords.forEach(record => {
            if (record.channel === track.channel) {
                numRacers = numRacers + 1;
            }
        });
        return numRacers >= 4;
    }
    
    public async leaveRace(message: Message, breakdown: boolean) {
        const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
        let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
        if (track != null) {
            if (track.trackActive) {
                let raceRecord = this.findRaceRecord(message.channel.id, message.member.id, message.guild.id)
                if (raceRecord != null) {
                    let index = this.raceRecords.indexOf(raceRecord);
                    this.raceRecords.splice(index, 1);
                    let messageToSend = breakdown ? `${message.member.displayName}'s vehicle breaks down, she just managed to get off the track and has to try again later...` : `(You have left this track without finishing, ${message.member.displayName}!)`
                    message.channel.send(messageToSend)
                } else {
                    message.channel.send(`(You are not running this track, ${message.member.displayName}!)`)
                }
            } else {
                message.channel.send(`(The track is currently not active, ${message.member.displayName}!)`)
            }
        } else {
            message.channel.send(`(No track is setup for this channel, ${message.member.displayName}!)`)
        }
        return;
    }

    public async raceStatus(message: Message) {
        const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
        let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
        if (track != null) {
            if (track.trackActive) {
                this.raceRecords.sort((a: RacingRecord, b: RacingRecord) => { return this.sortRacers(a,b,track.trackLength)})
                let participantString = ``;
                for (let i = 0; i < this.raceRecords.length; i++) {
                    participantString = participantString + `#${i + 1} - ${this.raceRecords[i].userName} - Lap: ${this.raceRecords[i].lap} Step: ${this.raceRecords[i].currentStep}/${track.trackLength} - Time: ${this.raceRecords[i].time}\n`
                }
                message.channel.send(`(***RACE STATUS***)\nCurrently racing:\n\`\`\`${participantString}\`\`\``)
            } else {
                message.channel.send(`(The track is currently not active, ${message.member.displayName}!)`)
            }
        } else {
            message.channel.send(`(No track is setup for this channel, ${message.member.displayName}!)`)
        }
        return;
    }

    public async driveRace(message: Message) {
        const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
        let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
        if (track != null) {
            if (track.trackActive) {
                let raceRecord = this.findRaceRecord(message.channel.id, message.member.id, message.guild.id)
                if (raceRecord != null) {
                    let paces = ["slow", "normal", "fast"];
                    let paceString = message.content.substring(14).toLowerCase();
                    if (!paces.includes(paceString)) {
                        message.channel.send(`(Invalid Input, ${message.member.displayName}, use either of \`slow; normal; fast\`!)`)
                    }
                    raceRecord.time = raceRecord.time + 1;
                    let movementRoll = Math.floor(Math.random() * (10 - 1 + 1) + 1);
                    let actualSpeed = raceRecord.speed;
                    let messageRolls = `Move: \`${paceString}\` Roll: \`${movementRoll}\``;
                    let messageIntent = ``;
                    let messageResult = ``;
                    // CHECK MOVEMENT
                    switch (paceString) {
                        case "slow":
                            actualSpeed = movementRoll == 10 ? actualSpeed : Math.ceil(actualSpeed * 0.75);
                            messageIntent = `drives carefully along the track`
                            messageResult = `and progresses slowly`
                            break;
                        case "normal":
                            let movementModifierN = 1.25;
                            messageIntent = `drives along the track`
                            messageResult = `and progresses quickly`
                            if (movementRoll === 1) {
                                movementModifierN = 0.25;
                                messageResult = `but the vehicle looses traction and progress is slow`
                            } else if (movementRoll === 10) {
                                movementModifierN = 1;
                                messageResult = `and progresses very quickly`
                            }
                            actualSpeed = Math.ceil(actualSpeed * movementModifierN);
                            break;
                        case "fast":
                            let movementModifierF = 1.25;
                            messageIntent = `drives along the track recklessly`
                            messageResult = `and progresses very quickly`
                            if (movementRoll === 1) {
                                movementModifierF = 0.1;
                                messageResult = `but looses control of the vehicle and make very little progress`
                            } else if (movementRoll === 10) {
                                movementModifierF = 1.5;
                                messageResult = `and progresses extremely quickly`
                            }
                            actualSpeed = Math.ceil(actualSpeed * movementModifierF);
                            break;
                    }
                    messageRolls = messageRolls + ` Dist: \`${actualSpeed}\``
                    let tilesMoved: number[] = [];
                    for (let i = raceRecord.currentStep + 1; i < raceRecord.currentStep + 1 + actualSpeed; i++) {
                        if (i > track.trackLength - 1) {
                            tilesMoved.push(i - track.trackLength);
                        } else {
                            tilesMoved.push(i);
                        }
                    }
                    
                    // DO CORNER STUFF
                    let actualTilesMoved: number[] = [];
                    let outOfRace = false;
                    let messageCorner = ``;
                    if (paceString !== "slow") {
                        let trackCorners: number[] = [];
                        let stringTrackCorners = track.trackCorners.split(";");
                        stringTrackCorners.forEach(stringTrackCorner => {
                            trackCorners.push(Number.parseInt(stringTrackCorner));
                        })
                        for (let i = 0; i < tilesMoved.length; i++) {
                            actualTilesMoved.push(tilesMoved[i]);
                            if (trackCorners.includes(tilesMoved[i])) {
                                // CORNER SKILL CHECK
                                let cornerRoll = Math.floor(Math.random() * (10 - 1 + 1) + 1);
                                messageRolls = messageRolls + ` Corner #${tilesMoved[i]}-Roll: \`${cornerRoll}\``
                                if (paceString === "normal" && cornerRoll === 1 && movementRoll !== 10) {
                                    messageCorner = ` - has trouble taking the corner and slows down`
                                    break;
                                }
                                if (paceString === "fast" && movementRoll === 1 && cornerRoll <= 5) {
                                    messageCorner = ` - has trouble taking the corner and slams into the sidewall`
                                    outOfRace = true;
                                    break; 
                                }
                                if (paceString === "fast" && movementRoll < 10 && cornerRoll <= 2) {
                                    messageCorner = ` - has trouble taking the corner and slows down`
                                    break;
                                }
                                if (paceString === "fast" && movementRoll === 10 && cornerRoll === 1) {
                                    messageCorner = ` - has trouble taking the corner and slows down`
                                    break;
                                }
                            }
                        }
                    } else {
                        actualTilesMoved = tilesMoved;
                    }
                    let finish = false;
                    raceRecord.currentStep = actualTilesMoved[actualTilesMoved.length -1];
                    if (actualTilesMoved.includes(0)) {
                        raceRecord.lap = raceRecord.lap + 1;
                        if (raceRecord.lap == 5) {
                            // End race
                            finish = true;
                            raceRecord.currentStep = 0;
                        }
                    }
                    message.channel.send(`*${message.member.displayName} ${messageIntent} - ${messageResult}${messageCorner}*\n(${messageRolls} - TrackPos: \`${raceRecord.currentStep + 1}/${track.trackLength}\`, Lap: \`${raceRecord.lap}\`, Time: \`${raceRecord.time})\``)
                    if (outOfRace) {
                        // Message end Race due to breakdown
                        this.leaveRace(message, true);
                        return;
                    }
                    if (finish) {
                        this.finishRace(message, raceRecord);
                        return;
                    }
                } else {
                    message.channel.send(`(You are not running this track, ${message.member.displayName}!)`)
                }
            } else {
                message.channel.send(`(The track is currently not active, ${message.member.displayName}!)`)
            }
        } else {
            message.channel.send(`(No track is setup for this channel, ${message.member.displayName}!)`)
        }
        return;
    }
    
    private async finishRace(message: Message, raceRecord: RacingRecord) {
        // Remove from active Racers
        let index = this.raceRecords.indexOf(raceRecord);
        this.raceRecords.splice(index, 1);

        const gupdtRaceTrackDao: GuPDTRacetrackDao = new GuPDTRacetrackDao();
        let track = await gupdtRaceTrackDao.getTrack(message.channel.id, message.guild.id);
        
        let racer: GuPDTRacer = null;
        for (let i = 0; i < track.racers.length; i++) {
            if (track.racers[i].racer === raceRecord.user) {
                racer = track.racers[i];
            }
        }
        if (racer === null) {
            racer = new GuPDTRacer();
            racer.attempts = 1;
            racer.bestTime = raceRecord.time;
            racer.racer = message.member.id;
            racer.track = track;
            racer.vehicleUsed = raceRecord.vehicle;
            track.racers.push(racer);
        } else {
            let index = track.racers.indexOf(racer);
            track.racers[index].attempts = racer.attempts + 1;
            track.racers[index].bestTime = raceRecord.time < racer.bestTime ? raceRecord.time : racer.bestTime;
            track.racers[index].vehicleUsed = raceRecord.vehicle;
        }
        track = await gupdtRaceTrackDao.updateTrack(track);

        track.racers.sort((a: GuPDTRacer, b: GuPDTRacer) => {
            if (a.bestTime < b.bestTime) {
                return -1;
            }
            if (a.bestTime > b.bestTime) {
                return 1;
            } 
            if (a.bestTime === b.bestTime) {
                if (a.attempts < b.attempts) {
                    return -1;
                }
                if (a.attempts > b.attempts) {
                    return 1;
                }
            }
            return 0;
        })
        let position = track.racers.indexOf(racer) + 1;
        message.channel.send(`(${message.member.displayName} finished the track and their current Position for ${track.trackName}'s ranking is **#${position}** with a time of **${racer.bestTime}**!)`)
    }

    private findRaceRecord(channel: string, member: string, guild: string): RacingRecord {
        for (let i = 0; i < this.raceRecords.length; i++) {
            let record = this.raceRecords[i];
            if (record.channel === channel && record.guild === guild && record.user === member) {
                return record;
            }
        }
        return null;
    }

    private isAdmin(author: GuildMember): boolean {
        let isAdmin = false;
        author.roles.cache.forEach((value: Role) => {
          if (value.name === "Admin" || value.name === "Technician") {
            isAdmin = true;
          }
        })
        return isAdmin;
    }

    private sortRacers(a: RacingRecord, b: RacingRecord, trackLength: number): number {
        let positionA = ((a.lap - 1) * trackLength) + a.currentStep;
        let positionB = ((b.lap - 1) * trackLength) + b.currentStep;
        if (positionA < positionB) {
            return 1;
        }
        if (positionA > positionB) {
            return -1;
        }
        if (positionA === positionB) {
            if (a.time < b.time) {
                return 1;
            }
            if (a.time > b.time) {
                return -1;
            }
        }
        return 0;
    }
}
