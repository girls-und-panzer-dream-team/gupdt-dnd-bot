import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { RacingCreatetrackCommand } from "./commands/racing-createtrack-command"
import { RacingDriveCommand } from "./commands/racing-drive-command"
import { RacingJoinCommand } from "./commands/racing-join-command"
import { RacingLeaderboardCommand } from "./commands/racing-leaderboard-command"
import { RacingToggletrackCommand } from "./commands/racing-toggletrack-command"
import { RacingRemovetrackCommand } from "./commands/racing-removetrack-command"
import { RacingLeaveCommand } from "./commands/racing-leave-command"
import { RacingStatusCommand } from "./commands/racing-status-command"
import { RacingToggleEventCommand } from "./commands/racing-toggleevent-command"
import { RacingHelpCommand } from "./commands/racing-help-command"
import { RacingVehiclesCommand } from "./commands/racing-vehicles-command"

export const RacingCommandGroup = new CommandGroup()
  .match(matchPrefixes("!racing"))
  .setCommands(RacingCreatetrackCommand, RacingDriveCommand, RacingJoinCommand, RacingJoinCommand, RacingLeaderboardCommand, 
               RacingToggletrackCommand, RacingRemovetrackCommand, RacingLeaveCommand, RacingStatusCommand, RacingToggleEventCommand,
               RacingHelpCommand, RacingVehiclesCommand)

