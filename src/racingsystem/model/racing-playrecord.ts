export class RacingRecord {
    guild: string
    channel: string;
    user: string;
    userName: string;
    lap: number;
    currentStep: number;
    time: number;
    speed: number;
    vehicle: string;
}
