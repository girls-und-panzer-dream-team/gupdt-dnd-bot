import { getConnection } from "typeorm";
import { GuPDTInvite } from "../entity/gupdt-invite";

export class GuPDTInviteDao {
    private connection = getConnection();

    public async getAllInvites(): Promise<GuPDTInvite[]> {
        return this.connection.manager.find(GuPDTInvite);
    }

    public async addInvite(invite: GuPDTInvite): Promise<GuPDTInvite> {
        return this.connection.manager.save(invite);
    }

    public async removeInvite(invite: GuPDTInvite) {
        return this.connection.manager.delete(GuPDTInvite, invite.id)
    }
}