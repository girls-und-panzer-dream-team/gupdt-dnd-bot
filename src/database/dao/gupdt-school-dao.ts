import { getConnection } from "typeorm";
import { GuPDTSchool } from "../entity/gupdt-school";

export class GuPDTSchoolDao {
    private connection = getConnection();

    public async getAllSchools(): Promise<GuPDTSchool[]> {
        return this.connection.manager.find(GuPDTSchool);
    }

    public async getSchoolBySchoolRole(schoolRole: string, guildid: string): Promise<GuPDTSchool> {
        const schools = await this.connection.manager.find(GuPDTSchool);
        for (let i = 0; i < schools.length; i++) {
            if (schools[i].guild == guildid && schools[i].schoolRole == schoolRole) {
                return schools[i];
            }
        }
    }

    public async addSchool(school: GuPDTSchool): Promise<GuPDTSchool> {
        return this.connection.manager.save(school);
    }
}
