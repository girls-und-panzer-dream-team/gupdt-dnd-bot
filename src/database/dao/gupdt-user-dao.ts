import { GuPDTUser } from "../entity/gupdt-user";
import { getConnection } from "typeorm";

export class GuPDTUserDao {
    private connection = getConnection();

    public async getAllUsers(): Promise<GuPDTUser[]> {
        return this.connection.manager.find(GuPDTUser);
    }

    public async getUser(userid: string, guildid: string): Promise<GuPDTUser> {
        const users = await this.connection.manager.find(GuPDTUser);
        for (let i = 0; i < users.length; i++) {
            if (users[i].guild == guildid && users[i].user == userid) {
                return users[i];
            }
        }
    }

    public async addUser(user: GuPDTUser): Promise<GuPDTUser> {
        return this.connection.manager.save(user);
    }
}
