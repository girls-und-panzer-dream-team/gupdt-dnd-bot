import { getConnection } from "typeorm";
import { GuPDTRacetrack } from "../entity/gupdt-racetrack";
import { GuPDTRacer } from "../entity/gupdt-racer";

export class GuPDTRacetrackDao {
    private connection = getConnection();

    public async getAllTracks(): Promise<GuPDTRacetrack[]> {
        return this.connection.manager.find(GuPDTRacetrack);
    }

    public async getTrack(channelid: string, guildid: string): Promise<GuPDTRacetrack> {
        const tracks = await this.connection.manager.find(GuPDTRacetrack);
        for (let i = 0; i < tracks.length; i++) {
            if (tracks[i].guild == guildid && tracks[i].channel == channelid) {
                return tracks[i];
            }
        }
        return null;
    }

    public async updateTrack(track: GuPDTRacetrack): Promise<GuPDTRacetrack> {
        return this.connection.manager.save(track);
    }

    public async removeTrack(track: GuPDTRacetrack) {
        track.racers.forEach(racer => {
            this.connection.manager.delete(GuPDTRacer, racer.id);
        })
        return this.connection.manager.delete(GuPDTRacetrack, track.id);
    }
}

