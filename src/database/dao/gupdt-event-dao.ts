import { getConnection } from "typeorm";
import { GuPDTEvent } from "../entity/gupdt-event";

export class GuPDTEventDao {
    private connection = getConnection();

    public async getAllEvents(): Promise<GuPDTEvent[]> {
        return this.connection.manager.find(GuPDTEvent);
    }

    public async getEvent(channelid: string, guildid: string): Promise<GuPDTEvent> {
        const events = await this.connection.manager.find(GuPDTEvent);
        for (let i = 0; i < events.length; i++) {
            if (events[i].guild == guildid && events[i].channel == channelid) {
                return events[i];
            }
        }
    }

    public async updateEvent(event: GuPDTEvent): Promise<GuPDTEvent> {
        return this.connection.manager.save(event);
    }
}