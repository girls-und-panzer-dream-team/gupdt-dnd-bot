import { GuPDTWallet } from "../entity/gupdt-wallet";
import { getConnection } from "typeorm";

export class GuPDTWalletDao {
    private connection = getConnection();

    public async getAllWallets(): Promise<GuPDTWallet[]> {
        return this.connection.manager.find(GuPDTWallet);
    }

    public async getWallet(userid: string, guildid: string): Promise<GuPDTWallet> {
        const wallets = await this.connection.manager.find(GuPDTWallet);
        for (let i = 0; i < wallets.length; i++) {
            if (wallets[i].guild == guildid && wallets[i].user == userid) {
                return wallets[i];
            }
        }
    }

    public async updateFunds(wallet: GuPDTWallet): Promise<GuPDTWallet> {
        return this.connection.manager.save(wallet);
    }

}