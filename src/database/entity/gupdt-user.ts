import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class GuPDTUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guild: string;

    @Column()
    user: string;

    @Column()
    school: string;

    @Column()
    battlesLost: number;

    @Column()
    battlesWon: number;

    @Column()
    blessings: number;
    
    @Column({type: "float"})
    crewXp: number;
}
