import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { GuPDTEvent } from "./gupdt-event";
@Entity()
export class GuPDTEventUser {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => GuPDTEvent, event => event.stages)
    event: GuPDTEvent;

    @Column()
    user: string;

    @Column()
    guild: string;

    @Column({type: "float"})
    activity: number;

    @Column({type: "float"})
    donations: number;

}
