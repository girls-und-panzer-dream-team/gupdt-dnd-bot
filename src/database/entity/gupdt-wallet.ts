import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class GuPDTWallet {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guild: string;

    @Column()
    user: string;
    
    @Column({type: "float"})
    balance: number;

    @Column()
    lastAllowanceMonth: number;

    @Column()
    lastAllowanceYear: number;
}
