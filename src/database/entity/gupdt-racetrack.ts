import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { GuPDTRacer } from "./gupdt-racer"

@Entity()
export class GuPDTRacetrack {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guild: string;

    @Column()
    channel: string;

    @Column()
    trackName: string;

    @Column()
    trackLength: number;

    @Column()
    trackCorners: string;

    @Column()
    trackActive: boolean;

    @Column()
    eventActive: boolean;
    
    @OneToMany(type => GuPDTRacer, racer => racer.track, { cascade: true, eager: true })
    racers: GuPDTRacer[];
}
