import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { GuPDTRacetrack } from "./gupdt-racetrack";

@Entity()
export class GuPDTRacer {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => GuPDTRacetrack, race => race.racers)
    track: GuPDTRacetrack;

    @Column()
    racer: string;

    @Column()
    bestTime: number;

    @Column()
    attempts: number;

    @Column()
    vehicleUsed: string;
}
