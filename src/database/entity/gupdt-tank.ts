import {Entity, Column, PrimaryColumn, ManyToOne} from "typeorm";
import { GuPDTSchool } from "./gupdt-school";

@Entity()
export class GuPDTTank {
    @PrimaryColumn()
    tankid: string;

    @ManyToOne(type => GuPDTSchool, school => school.tanks)
    school: GuPDTSchool

    @Column()
    owner: string;

    @Column()
    tankName: string;

    @Column()
    tracked: boolean;
    
    @Column()
    damageTotal: number;
    @Column()
    damageHull: number;
    @Column()
    damageTurret: number;
    @Column()
    damageSide: number;
    @Column()
    damageWeakspot: number;
}
