import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { GuPDTEvent } from "./gupdt-event";

@Entity()
export class GuPDTEventStage {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => GuPDTEvent, event => event.stages)
    event: GuPDTEvent;

    @Column()
    stageName: string;

    @Column()
    stageType: string;

    @Column()
    stageDescription: string;

    @Column()
    stageActive: boolean;

    @Column({type: "float"})
    stageProgress: number;

    @Column({type: "float"})
    stageTarget: number;

}
