import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { GuPDTEventStage } from "./gupdt-eventstage";
import { GuPDTEventUser } from "./gupdt-eventuser";

@Entity()
export class GuPDTEvent {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guild: string;

    @Column()
    channel: string;

    @Column()
    eventName: string;

    @Column()
    eventCreator: string;

    @Column()
    active: boolean;
    
    @OneToMany(type => GuPDTEventStage, stage => stage.event, { cascade: true, eager: true })
    stages: GuPDTEventStage[];

    @OneToMany(type => GuPDTEventUser, stage => stage.event, { cascade: true, eager: true })
    users: GuPDTEventUser[];
}
