import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class GuPDTInvite {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guild: string;

    @Column()
    user: string;

    @Column()
    inviteRole: string;

    @Column({type: "datetime"})
    inviteDate: Date;
}
