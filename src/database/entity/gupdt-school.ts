import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { GuPDTTank } from './gupdt-tank'

@Entity()
export class GuPDTSchool {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guild: string;

    @Column()
    schoolName: string;

    @Column()
    schoolRole: string;

    @Column()
    schoolScore: number; 

    @OneToMany(type => GuPDTTank, tank => tank.school, { cascade: true, eager: true })
    tanks: GuPDTTank[];
}
