import { Service } from "@enitoni/gears-discordjs";
import { Battle6D } from "./model/battle";
import { TextChannel, GuildMember, Message } from "discord.js";
import { tankStats } from "./tanklist.json";
import { Tank6D } from "./model/tank";
import { StoredDamageProcedure6D } from "./model/special/stored-damage-procedure" 
import { BattleMiscFunctions6D } from "./helpers/battle-misc-functions";
import { BattlePreparationFunctions6D } from "./helpers/battle-prep-functions";
import { BattleDataFunctions6D } from "./helpers/battle-data-functions";
import { BattleMessages6D } from "./messages/battle-messages";
import { SpecialMessages6D } from "./messages/battle-special-messages";

export class BattleMain6D extends Service {
  public channels: string[] = [];  
  // LIST OF ACTIVE BATTLES
  public battles: Battle6D[] = [];

  // HELPER FOR IN-BATTLE FUNCTIONS
  private helper: BattleMiscFunctions6D = new BattleMiscFunctions6D();

  // HELPER FOR BATTLE-PREP FUNCTIONS
  private battlePreparationFunctions: BattlePreparationFunctions6D = new BattlePreparationFunctions6D();

  // HELPER FOR SIMPLE DATA MAPPING
  private battleDataFunctions: BattleDataFunctions6D = new BattleDataFunctions6D();

  private battleMessages: BattleMessages6D = new BattleMessages6D();
  private specialMessages: SpecialMessages6D = new SpecialMessages6D();

  /********************************************************/
  /*                   BATTLE PREP FUNCTIONS              */
  /********************************************************/
  public noBattleActive(channel: TextChannel): boolean {
    let battle = this.helper.findBattle(channel, this.battles)
    return battle === null;
  }

  // PREPARE BATTLE FOR COMMANDERS TO JOIN
  public prepareBattle(channel: TextChannel, author: GuildMember, message: Message): boolean {
    return this.battlePreparationFunctions.prepareBattle(channel, author, this.battles, message);
  }

  // JOIN BATTLE AS COMMANDER
  public async joinBattle(channel: TextChannel, author: GuildMember, content: string, message: Message): Promise<string> {
    return this.battlePreparationFunctions.joinBattle(channel, author, content, this.battles);
  }

  // LEAVE BATTLE PREPERATION
  public leaveBattle(channel: TextChannel, author: GuildMember): string {
    return this.battlePreparationFunctions.leaveBattle(channel, author, this.battles);
  }

  // CANCEL BATTLE
  public cancelBattle(channel: TextChannel, author: GuildMember): string {
    return this.battlePreparationFunctions.cancelBattle(channel, author, this.battles);
  }

  // START BATTLE
  public startBattle(channel: TextChannel, author: GuildMember): boolean {
    return this.battlePreparationFunctions.startBattle(channel, author, this.battles);
  }

  /********************************************************/
  /*                 BATTLE STATUS FUNCTIONS              */
  /********************************************************/
  // CHECK IF BATTLE CAN BE STARTED
  public canStartBattle(channel: TextChannel): boolean {
    let battle = this.helper.findBattle(channel, this.battles);
    if (battle != null) {
      return battle.teamA.length > 0 && battle.teamB.length > 0;
    }
    return false;
  }

  // END BATTLE
  private endBattle(battle: Battle6D, winner: string) {
    // Save Winner User Data
    let winningTeam = winner === "Team B" ? battle.teamB : battle.teamA;

    // Send Winner announcement
    let looser = winner === "Team B" ? "Team A" : "Team B";
    battle.channel.send(this.battleMessages.announceWinner(looser, winner));
    this.battleStatus(battle.channel);
    
    // Remove the ended Battle
    let index = this.battles.indexOf(battle);
    this.battles.splice(index, 1);
  }

  /********************************************************/
  /*                   BATTLE COMMANDS                    */
  /********************************************************/

  // CHECK STATUS OF BATTLE 
  public battleStatus(channel: TextChannel) {
    let battle = this.helper.findBattle(channel, this.battles);
      if (battle != null && battle.started && battle.round > 0) {
        this.battleMessages.displayTeams(battle);
      }
  }

  // END PARTICIPATION BY SURRENDERING
  public surrender(channel: TextChannel, author: GuildMember) {
    let battle = this.helper.findBattle(channel, this.battles);
    if (battle != null && battle.started && battle.round > 0 && battle.participants.includes(author) && battle.confirmant == null) {
      if (battle.turnOrder[0].commander.id !== author.id) {
        channel.send(this.battleMessages.notYourTurn(author));
        return;
      }
      let tank = battle.turnOrder[0];

      this.helper.tankDestroyed(battle, tank);
      channel.send(this.battleMessages.surrender(tank))
      
      let endBattle = this.helper.checkForEndOfBattle(battle);
      if (endBattle != null) {
        this.endBattle(battle, endBattle);
        return;
      }
      
      this.helper.checkEndTurn(battle);
      this.helper.notifyCommander(battle.turnOrder[0], true, battle)
    }
  }

  // SKIP THE REMAINDER OF THE TURN
  public skip(channel: TextChannel, author: GuildMember) {
    let battle = this.helper.findBattle(channel, this.battles);

    if (battle != null && battle.started && battle.round > 0 && battle.participants.includes(author) && battle.confirmant == null) {
      if (battle.turnOrder[0].commander.id !== author.id) {
        channel.send(this.battleMessages.notYourTurn(author));
        return;
      }
      this.endOfAction(battle);
    }
  }

  // USE SPECIAL ABILITY (IF APPLICABLE)
  public special(channel: TextChannel, author: GuildMember, aiTank: Tank6D) {
    let battle = this.helper.findBattle(channel, this.battles);

    if (battle != null && battle.started && battle.round > 0 && battle.participants.includes(author) && battle.confirmant == null) {
      if (battle.turnOrder[0].commander.id !== author.id) {
        channel.send(this.battleMessages.notYourTurn(author));
        return;
      }

      let tank = aiTank == null ? this.helper.findTank(battle, author) : aiTank;;
      if (tank.specialUsed) {
        channel.send(this.specialMessages.specialUsed(author));
        return;
      }
      if (tank.cooldown) {
        channel.send(this.specialMessages.cooldownWarning(author, tank));
        return;
      }
      // SPECIAL RULE: DEVILISH SPEED
      if (tank.special.includes("DevilishSpeed")) {
        tank.specialUsed = true;
        tank.clearSpecial = true;
        tank.hitThreshhold = 10;
        channel.send(this.specialMessages.devilishSpeedActivate(author));
        return;
      }
      // SPECIAL RULE: MASS PRODUCED
      if (tank.special.includes("MassProduced")) {
        if (tank.health >= tankStats[tank.tankName].health) {
          channel.send(this.specialMessages.massProducedActivate(author));
          return;
        }
        tank.specialUsed = true;
        tank.health = tank.health + 1;
        channel.send(this.specialMessages.massProducedActivate(author));
        return;
      }
      // SPECIAL RULE: DUCK POWER
      if (tank.special.includes("DuckPower")) {
        tank.specialUsed = true;
        tank.hitThreshhold = 7;
        channel.send(this.specialMessages.duckPowerActivate(author));
        return;
      }
      // SPECIAL RULE: STEADY...
      if (tank.special.includes("Steady")) {
        tank.specialUsed = true;
        tank.cooldown = 2;
        channel.send(this.specialMessages.steadyActivate(author));
        this.endOfAction(battle);
        return;
      }
      // SPECIAL RULE: SPACED STEEL
      if (tank.special.includes("SpacedSteel")) {
        tank.specialUsed = true;
        tank.health = tank.health + 1.5
        channel.send(this.specialMessages.spacedSteelActivate(author));
        return;
      }
    }
  }

  /********************************************************/
  /*          BATTLE COMMANDS - COMBAT FLOW               */
  /********************************************************/
  // ROLL FOR INITIATIVE
  public rollInitiative(channel: TextChannel, author: GuildMember) {
    let battle = this.helper.findBattle(channel, this.battles);
    
    if (battle != null && battle.started && battle.round === 0 && battle.participants.includes(author)) {
      let tank = this.helper.findTank(battle, author);

      if (battle.turnOrder.includes(tank)) {
        channel.send(this.battleMessages.initiativeWarn(author))
        return;
      }

      let initiativeRoll = this.battleDataFunctions.rollDice(1, 10);

      // SPECIAL RULE: THIS IS WHERE THE FUN BEGINS
      if (tank.special.includes("ThisIsWhereTheFunBegins") || tank.special.includes("SandSweptHorizon")) {
        let bonus = 2;
        if (tank.special.includes("ThisIsWhereTheFunBegins")) {
          bonus = 10;
        }
        channel.send(this.specialMessages.thisIsWhereTheFunBeginsMessage(bonus))
        initiativeRoll = initiativeRoll + bonus;
      }

      tank.initative = initiativeRoll;

      channel.send(this.battleMessages.initiativeRoll(tank.commanderName, initiativeRoll));
      
      battle.turnOrder.push(tank);

      if (battle.turnOrder.length === battle.participants.length) {
        battle.turnOrder.sort((a: Tank6D, b: Tank6D) => { return this.battleDataFunctions.sortByInitiative(a,b)})
        battle.round = battle.round + 1;
        this.battleMessages.openBattleMessage(battle)
        this.helper.notifyCommander(battle.turnOrder[0], true, battle);
      } 
    }
    return;
  }

  // INITIAL COMMAND TO FIRE AT A TANK
  public targetTank(channel: TextChannel, author: GuildMember, content: string, aiTank: Tank6D) {
    let battle = this.helper.findBattle(channel, this.battles);
    if (aiTank != null || battle != null && battle.started && battle.round > 0 && battle.participants.includes(author) && battle.confirmant == null) {
      if (aiTank == null && battle.turnOrder[0].commander.id !== author.id) {
        channel.send(this.battleMessages.notYourTurn(author));
        return;
      }

      // CLEAN ARGS
      let cleanString = content.substring(15).replace(/\s+/g, " ").trim();
      let args = cleanString.split(" ");
      let firingTank = aiTank == null ? this.helper.findTank(battle, author) : aiTank;
      let targetTank = this.helper.findTankByTargetId(battle, args[0].toUpperCase());

      // CHECK VALID TARGET
      if (targetTank == null) {
        channel.send(this.battleMessages.invalidTargetWarn(args))
        return;
      }

      if (firingTank.targetId === targetTank.targetId) {
        channel.send(this.battleMessages.suicideWarn(author))
        return;
      }

      // AIM CHECK
      let aimingModifier = 0;
      let isAiming = "";
      if (args.length > 1) {
        let possibleTargets = ["hull", "turret", "tracks", "side", "weakspot"];
        if (!possibleTargets.includes(args[1].toLowerCase())) {
          channel.send(this.battleMessages.invalidAimWarn())
          return;
        }
        if (firingTank.tracked && !firingTank.special.includes("IAmSpeed")) {
          channel.send(this.battleMessages.cantAimCauseTracked())
          return;
        }
        // SPECIAL RULE: DUCK POWER
        if (firingTank.specialUsed && firingTank.special.includes("DuckPower")) {
          channel.send(this.specialMessages.cantAimDuckPower())
          return;
        }
        // SPECIAL RULE: SLOW BUT SURE
        if (firingTank.special.includes("SlowButSure")) {
          channel.send(this.specialMessages.cantAimSlowButSure())
          return;
        }

        // SPECIAL RULE: MY AIM IS TRUE
        aimingModifier = firingTank.special.includes("MyAimIsTrue") ? 2 : 3;
        isAiming = `while aiming at the ${args[1].toLowerCase()}`;
      }

      // HIT CHECK
      let aimingTarget = aimingModifier + targetTank.hitThreshhold;
      let hitRoll = this.battleDataFunctions.rollDice(1, 10);

      // SPECIAL RULE: STEADY...
      if (firingTank.specialUsed && firingTank.special.includes("Steady")) {
        firingTank.specialUsed = false;
        hitRoll = hitRoll + 2;
        channel.send(this.specialMessages.steadyExecute(author, hitRoll));
      }

      // SPECIAL RULE: YOU'VE HAD YOUR SIX / TRY TRY AGAIN
      if (hitRoll < aimingTarget && firingTank.cooldown === 0 && (firingTank.special.includes("YouVeHadYourSix") || firingTank.special.includes("TryTryAgain"))) {
        let specialRuleString = firingTank.special.includes("YouVeHadYourSix") ? "YOU'VE HAD YOUR SIX" : "TRY TRY AGAIN";
        channel.send(this.specialMessages.rerollHitMessage(firingTank.commanderName, targetTank, isAiming, aimingTarget, hitRoll, specialRuleString));
        firingTank.cooldown = firingTank.special.includes("YouVeHadYourSix") ? 1 : 2;
        this.helper.notifyCommander(battle.turnOrder[0], false, battle);
        return;
      }

      let hitResult = hitRoll >= aimingTarget ? "hits" : "misses";
      channel.send(this.battleMessages.hitCheckMessage(firingTank.commanderName, targetTank, isAiming, aimingTarget, hitRoll, hitResult));
      
      if (hitRoll >= aimingTarget) {
        // SPECIAL RULE: SIZE MATTERS
        if (firingTank.special.includes("SizeMatters") && tankStats[targetTank.tankName].health <= 5) {
          channel.send(this.specialMessages.sizeMattersExecute());
          let skip = this.reduceHealth(targetTank, targetTank.health, channel, battle);
          if (skip) {
            return
          }
          this.endOfAction(battle);
          return;
        }

        // HIT LOCATION
        let hitLocation = "";
        if (args.length > 1) {
          hitLocation = args[1].toLowerCase();
          channel.send(this.battleMessages.hitAimLocationMessage(firingTank.commanderName, targetTank, hitLocation));
        } else {
          let hitLocationRoll = this.battleDataFunctions.rollDice(1, 10);
          hitLocation = this.battleDataFunctions.findHitLocation(hitLocationRoll)
          channel.send(this.battleMessages.hitRandomLocationMessage(firingTank.commanderName, targetTank, hitLocation, hitLocationRoll));
          
          // SPECIAL RULE: SHOW ME WHERE IT HURTS
          if (firingTank.cooldown === 0 && firingTank.special.includes("ShowMeWhereItHurts") && battle.storedDamageProcedure === null) {
            battle.confirmant = firingTank;
            battle.storedDamageProcedure = new StoredDamageProcedure6D();
            battle.storedDamageProcedure.author = author;
            battle.storedDamageProcedure.channel = channel;
            battle.storedDamageProcedure.firingTank = firingTank;
            battle.storedDamageProcedure.hitLocation = hitLocation;
            battle.storedDamageProcedure.targetTank = targetTank;
            battle.storedDamageProcedure.specialRuleApplied = "ShowMeWhereItHurts";
            battle.storedDamageProcedure.entrypoint = "doDamage";
            channel.send(this.specialMessages.showMeWhereItHurtsQuestion(firingTank))
            return;
          }
        }
        let skip = this.doDamage(targetTank, firingTank, hitLocation, channel, battle, author);
        
        if (skip) {
          return;
        }
      }
      this.endOfAction(battle);
    }
    return;
  }
 
  // CONTINUE AT ROLL FOR DAMAGE
  private doDamage(targetTank: Tank6D, firingTank: Tank6D, hitLocation: string, channel: TextChannel, battle: Battle6D, author: GuildMember): boolean {
    // DAMAGE CHECK
    // SPECIAL RULE: OL RELIABLE
    if (hitLocation ==="weakspot" && targetTank.special.includes("LuckOfTheFool")){
      let avoidRoll = this.battleDataFunctions.rollDice(1, 100);
      if (avoidRoll > 60) {
        hitLocation = "hull";
      }
      channel.send(this.specialMessages.luckOfTheFool(avoidRoll, hitLocation));
    }
    if (!targetTank.specialUsed && hitLocation === "weakspot" && targetTank.special.includes("OlReliable") && battle.storedDamageProcedure == null) {
      battle.confirmant = targetTank;
      battle.storedDamageProcedure = new StoredDamageProcedure6D();
      battle.storedDamageProcedure.author = author;
      battle.storedDamageProcedure.channel = channel;
      battle.storedDamageProcedure.firingTank = firingTank;
      battle.storedDamageProcedure.hitLocation = hitLocation;
      battle.storedDamageProcedure.targetTank = targetTank;
      battle.storedDamageProcedure.specialRuleApplied = "OlReliable";
      battle.storedDamageProcedure.entrypoint = "doDamage";
      channel.send(this.specialMessages.olReliableQuestion(targetTank))
      return true;

    } else if (!targetTank.tracked && hitLocation === "tracks") {
      targetTank.tracked = true;
      // SPECIAL RULE: I AM SPEED
      if (targetTank.special.includes("IAmSpeed")) {
        targetTank.hitThreshhold = targetTank.hitThreshhold + 2;
        channel.send(this.specialMessages.specialTrackHitMessage(targetTank));
      } else {
        targetTank.hitThreshhold = targetTank.hitThreshhold - 2;
        channel.send(this.battleMessages.trackHitMessage(targetTank));
      }
    } else {
      let locationDamage = this.battleDataFunctions.checkLocationDamage(hitLocation, targetTank, firingTank);
      let damageRoll = this.battleDataFunctions.rollDice(1, 100);
      // SPECIAL RULE: SPACING IS KEY
      if (targetTank.special.includes("SpacingIsKey")) {
        channel.send(this.specialMessages.spacingIsKeyExecute())
        damageRoll = damageRoll + 20;
      }
      // SPECIAL RULE: TRAFALAGER'S PRIDE
      if (firingTank.special.includes("TrafalagersPride") && targetTank.type === "light") {
        channel.send(this.specialMessages.trafalagersPrideExecute())
        damageRoll = damageRoll + 15;
      }
      // SPECIAL RULE: PUMP YOU UP
      if (firingTank.special.includes("PumpYouUp")) {
        channel.send(this.specialMessages.pumpYouUpExecute())
        damageRoll = damageRoll + 15;
      }
      // SPECIAL RULE: FRENCH COMPETANCE
      if (firingTank.special.includes("FrenchCompetance")) {
        channel.send(this.specialMessages.frenchCompetanceExecute())
        damageRoll = damageRoll - 5;
      }
      // SPECIAL RULE: A DASH OF OOMPH
      if (firingTank.special.includes("ADashOfOomph")) {
        channel.send(this.specialMessages.aDashOfOomphExecute())
        damageRoll = damageRoll + 10;
      }
      // SPECIAL RULE: DOWNUNDER POINT OF VIEW
      if (((firingTank.special.includes("DownunderPointOfView") || firingTank.special.includes("PatrioticPunch") || firingTank.special.includes("MemoriesOfRaseiniai")) && !firingTank.specialUsed && battle.storedDamageProcedure == null) || (!targetTank.specialUsed && targetTank.special.includes("StrengthOfLegions"))) {
        battle.confirmant = firingTank;
        battle.storedDamageProcedure = new StoredDamageProcedure6D();
        battle.storedDamageProcedure.author = author;
        battle.storedDamageProcedure.channel = channel;
        battle.storedDamageProcedure.firingTank = firingTank;
        battle.storedDamageProcedure.hitLocation = hitLocation;
        battle.storedDamageProcedure.targetTank = targetTank;
        battle.storedDamageProcedure.damageroll = damageRoll;
        battle.storedDamageProcedure.entrypoint = "applyDamage"
        battle.storedDamageProcedure.locationDamage = locationDamage;
        if (firingTank.special.includes("DownunderPointOfView")) {
          let stringNumber = damageRoll.toString().split('').reverse().join('')
          let reverseNumber = parseInt(stringNumber)
          if (reverseNumber > damageRoll) {
            battle.storedDamageProcedure.specialRuleApplied = "DownunderPointOfView";
            channel.send(this.specialMessages.downUnderPointOfViewQuestion(firingTank, damageRoll, reverseNumber))
            return true;
          }
        }
        // SPECIAL RULE: PATRIOTIC PUNCH
        if (firingTank.special.includes("PatrioticPunch") && damageRoll < 76) {
          battle.storedDamageProcedure.specialRuleApplied = "PatrioticPunch";
          channel.send(this.specialMessages.patrioticPunchQuestion(firingTank, damageRoll))
          return true;
        }
        // SPECIAL RULE: MEMORIES OF RASEINIAI
        if (firingTank.special.includes("MemoriesOfRaseiniai") && damageRoll < 75 && firingTank.cooldown === 0) {
          battle.storedDamageProcedure.specialRuleApplied = "MemoriesOfRaseiniai";
          channel.send(this.specialMessages.memoriesOfRaseiniaiQuestion(firingTank, damageRoll))
          return true;
        }
        // SPECIAL RULE: STRENGTH OF LEGIONS
        if (targetTank.special.includes("StrengthOfLegions") && damageRoll > 75 && !targetTank.specialUsed) {
          battle.confirmant = targetTank;
          battle.storedDamageProcedure.specialRuleApplied = "StrengthOfLegions";
          channel.send(this.specialMessages.strengthOfLegionsQuestion(targetTank, damageRoll))
          return true;
        }
        battle.storedDamageProcedure = null;
        battle.confirmant = null; 
      }
      let skip = this.applyDamage(damageRoll, firingTank, targetTank, channel, hitLocation, locationDamage, author, battle);
      if (skip) {
        return true;
      }
    }
    battle.storedDamageProcedure = null
    return false;
  }

  // CONTINUE AT APPLYING DAMAGE
  private applyDamage(damageRoll: number, firingTank: Tank6D, targetTank: Tank6D, channel: TextChannel, hitLocation: string, locationDamage: number, author: GuildMember, battle: Battle6D): boolean {
    let damagePoints = this.battleDataFunctions.findDamagePointValue(damageRoll);
    // SPECIAL RULE: A THOUSAND CUTS
    if (firingTank.special.includes("AThousandCuts") && damagePoints > 2) {
      damagePoints = 2;
      channel.send(this.specialMessages.aThousandCutsExecute());
    }
    // SPECIAL RULE: TERRIBLE ROUNDS
    if (firingTank.special.includes("TerribleRounds") && targetTank.type !== "light") {
      damagePoints = 1.5;
      channel.send(this.specialMessages.terribleRoundsExecute());
    }

    let damageMultiplier = hitLocation === "weakspot" ? 2 : 1;
    let damagePointsFinal = (damagePoints + locationDamage) * damageMultiplier;

    // SPECIAL RULE: A LITTLE OFF THE TOP
    if (targetTank.special.includes("ALittleOfTheTop") && hitLocation === 'turret') {
      damagePointsFinal = damagePointsFinal * 1.5;
      channel.send(this.specialMessages.aLittleOffTheTopExecute())
    }
    // SPECIAL RULE: DAKKA GUNS
    if (firingTank.special.includes("DakkaGuns")) {
      channel.send(this.specialMessages.dakkaGunsExecute())
      damagePointsFinal = 0.5;
    }
    // SPECIAL RULE: BASED TANKING
    if (firingTank.special.includes("BasedTanking") && (hitLocation === "turret" || hitLocation === "hull" || hitLocation === "tracks")) {
      channel.send(this.specialMessages.basedTankingExecute())
      damagePointsFinal = 1;
    }
    // SPECIAL RULE: SPACING IS KEY
    if (targetTank.special.includes("SpacingIsKey") && (hitLocation === "hull" || hitLocation === "tracks")) {
      channel.send(this.specialMessages.spacingIsKeyExecute2())
      damagePointsFinal = damagePointsFinal / 2;
    }
    // SPECIAL RULE: BIG BEASTS CLAW
    if (firingTank.special.includes("BigBeastsClaw")) {
      channel.send(this.specialMessages.bigBeastsClawExecute())
      damagePointsFinal = damagePointsFinal + 1;
    }
    // SPECIAL RULE: PAPER WALLS
    if (targetTank.special.includes("PaperWalls")) {
      if (hitLocation === "hull") {
        channel.send(this.specialMessages.paperWallsExecuteHull());
        damagePointsFinal = damagePointsFinal * 1.5;
      }
      if (hitLocation === "side" || hitLocation === "weakspot") {
        channel.send(this.specialMessages.paperWallsExecuteSide());
        damagePointsFinal = 0;
      }
    }
    // SPECIAL RULE: THE RIGHT PLACES
    if (targetTank.special.includes("TheRightPlaces")) {
      if (hitLocation === "hull" || hitLocation === "turret") {
        channel.send(this.specialMessages.theRightPlacesExecuteTurret());
        damagePointsFinal = damagePointsFinal * 0.5;
      }
      if (hitLocation === "side" || hitLocation === "weakspot") {
        channel.send(this.specialMessages.theRightPlacesExecuteSide());
        damagePointsFinal = damagePointsFinal * 2;
      }
    }

    let damageString = this.battleDataFunctions.findDamageStringValue(damagePointsFinal);
    channel.send(this.battleMessages.damageMessage(firingTank.commanderName, damageString, damageRoll, damagePoints, locationDamage, damageMultiplier, damagePointsFinal));
    channel.send(this.battleMessages.healthRemainingMessage(targetTank, damageString, damagePointsFinal));
    
    if (!targetTank.specialUsed && targetTank.special.includes("RussianBias")) {
      battle.confirmant = targetTank;
      battle.storedDamageProcedure = new StoredDamageProcedure6D();
      battle.storedDamageProcedure.author = author;
      battle.storedDamageProcedure.channel = channel;
      battle.storedDamageProcedure.targetTank = targetTank;
      battle.storedDamageProcedure.firingTank = firingTank;
      battle.storedDamageProcedure.entrypoint = "reduceHealth"
      battle.storedDamageProcedure.specialRuleApplied = "RussianBias";
      battle.storedDamageProcedure.finalDamage = damagePointsFinal;
      battle.storedDamageProcedure.hitLocation = hitLocation;
      channel.send(this.specialMessages.russianBiasQuestion(targetTank));
      return true;
    }

    let skip = this.reduceHealth(targetTank, damagePointsFinal, channel, battle);
    if (skip) {
      return true;
    }

    return false;
  }

  // CONTINUE AT REDUCE HEALTH
  public reduceHealth(targetTank: Tank6D, damagePointsFinal: number, channel: TextChannel, battle: Battle6D) : boolean{
    // APPLY DAMAGE
    targetTank.health = targetTank.health - damagePointsFinal;
    
    // SPECIAL RULE: PLOT ARMOR
    if (targetTank.health <= 0 && targetTank.special.includes("PlotArmor") && !targetTank.specialUsed) {
      let safeRoll = this.battleDataFunctions.rollDice(1, 100);
      let result = " is taken out anyway"
      if (safeRoll > 70) {
        targetTank.specialUsed = true;
        targetTank.health = 1;
        result = " heals 1 Health"
      }
      channel.send(this.specialMessages.plotArmorExecute(result, safeRoll));
    }
    if (targetTank.health <= 0) {
      this.helper.tankDestroyed(battle, targetTank);
      channel.send(this.battleMessages.tankDestroyedMessage(targetTank));
      let endBattle = this.helper.checkForEndOfBattle(battle);
      if (endBattle != null) {
        this.endBattle(battle, endBattle);
        return true;
      }
    }
    return false;
  }

  // EXECUTED AT END OF PLAYER ACTION
  private endOfAction(battle: Battle6D) {
    let tank = battle.turnOrder[0];
    if (!tank.specialUsed && tank.special.includes("DesertSnakes") && battle.storedDamageProcedure === null) {
      battle.confirmant = tank;
      battle.storedDamageProcedure = new StoredDamageProcedure6D();
      battle.storedDamageProcedure.specialRuleApplied = "DesertSnakes";
      battle.storedDamageProcedure.entrypoint = "restart";
      battle.storedDamageProcedure.firingTank = tank;
      battle.channel.send(this.specialMessages.desertSnakesQuestion(tank));
      return;
    }
    battle.storedDamageProcedure = null;
    battle.turnDone.push(battle.turnOrder.splice(0,1)[0]);
    
    // CHECK FOR END OF TURN
    this.helper.checkEndTurn(battle);
    this.helper.notifyCommander(battle.turnOrder[0], true, battle)
  }

  /********************************************************/
  /*        BATTLE COMMANDS - CONFIRMATION SYSTEM         */
  /********************************************************/
  // CONFIRM EXECUTING ACTION PROPOSED BY THE BOT
  public confirm(channel: TextChannel, author: GuildMember) {
    let battle = this.helper.findBattle(channel, this.battles);
    if (battle.confirmant && battle.confirmant.commander === author) {
      battle.confirmant = null;
      if (battle.storedDamageProcedure != null) {
        switch(battle.storedDamageProcedure.specialRuleApplied) {
          case "OlReliable":
            battle.storedDamageProcedure.targetTank.specialUsed = true;
            battle.storedDamageProcedure.hitLocation = "tracks";
            break;
          case "DownunderPointOfView":
            battle.storedDamageProcedure.firingTank.specialUsed = true;
            let stringNumber = battle.storedDamageProcedure.damageroll.toString().split('').reverse().join('')
            battle.storedDamageProcedure.damageroll = parseInt(stringNumber);
            break;
          case "DesertSnakes":
            battle.storedDamageProcedure.firingTank.specialUsed = true;
            battle.storedDamageProcedure.firingTank.health = battle.storedDamageProcedure.firingTank.health - 1.5;
            battle.channel.send(this.specialMessages.desertSnakesExecute(battle));
            break;
          case "RussianBias":
            battle.storedDamageProcedure.targetTank.specialUsed = true;
            battle.storedDamageProcedure.finalDamage = 0;
            break;
          case "PatrioticPunch":
            battle.storedDamageProcedure.firingTank.specialUsed = true;
            battle.storedDamageProcedure.damageroll = 76
            break;
          case "MemoriesOfRaseiniai":
            battle.storedDamageProcedure.firingTank.cooldown = 2;
            battle.storedDamageProcedure.damageroll = this.battleDataFunctions.rollDice(1, 100);
            break;
          case "ShowMeWhereItHurts":
            let hitLocationRoll = this.battleDataFunctions.rollDice(1, 10);
            battle.storedDamageProcedure.firingTank.cooldown = 2;
            battle.storedDamageProcedure.hitLocation = this.battleDataFunctions.findHitLocation(hitLocationRoll);
            battle.channel.send(this.specialMessages.showMeWhereItHurtsExecute(battle, hitLocationRoll));
            break;
          case "StrengthOfLegions":
            battle.storedDamageProcedure.damageroll = this.battleDataFunctions.rollDice(1, 100);
            battle.channel.send(this.specialMessages.strengthOfLegionsExecute(battle));
            battle.storedDamageProcedure.targetTank.specialUsed = true;
            break;
        }
        if (battle.storedDamageProcedure.entrypoint === "doDamage") {
          this.continueExecuteDoDamage(battle);
        } else if (battle.storedDamageProcedure.entrypoint === "applyDamage") {
          this.continueApplyDamage(battle);
        } else if (battle.storedDamageProcedure.entrypoint === "reduceHealth") {
          this.continueReduceHealth(battle);
        } else if (battle.storedDamageProcedure.entrypoint === "restart") {
          battle.storedDamageProcedure = null;
          this.helper.notifyCommander(battle.turnOrder[0], true, battle)
        }
      }
    }
  }
  
  // DENY EXECUTING ACTION PROPOSED BY THE BOT
  public deny(channel: TextChannel, author: GuildMember) {
    let battle = this.helper.findBattle(channel, this.battles);
    if (battle.confirmant && battle.confirmant.commander === author) {
      battle.confirmant = null;
      if (battle.storedDamageProcedure != null) {
        if (battle.storedDamageProcedure.entrypoint === "doDamage") {
          this.continueExecuteDoDamage(battle);
        } else if (battle.storedDamageProcedure.entrypoint === "applyDamage") {
          this.continueApplyDamage(battle);
        } else if (battle.storedDamageProcedure.entrypoint === "reduceHealth") {
          this.continueReduceHealth(battle);
        } else if (battle.storedDamageProcedure.entrypoint === "restart") {
          this.endOfAction(battle);
        }
      }
    }
  }
  
  // CONTINUE WITH APPLY DAMAGE AFTER A CONFIRMREQUEST
  private continueApplyDamage(battle: Battle6D) {
    if (battle.storedDamageProcedure != null) {
      this.applyDamage(battle.storedDamageProcedure.damageroll, battle.storedDamageProcedure.firingTank, battle.storedDamageProcedure.targetTank, battle.storedDamageProcedure.channel, battle.storedDamageProcedure.hitLocation, battle.storedDamageProcedure.locationDamage, battle.storedDamageProcedure.author, battle);
      this.endOfAction(battle);
    }
  }

  // CONTINUE WITH DO DAMAGE AFTER A CONFIRMREQUEST
  private continueExecuteDoDamage(battle: Battle6D) {
    if (battle.storedDamageProcedure != null) {
      this.doDamage(battle.storedDamageProcedure.targetTank, battle.storedDamageProcedure.firingTank, battle.storedDamageProcedure.hitLocation,
        battle.storedDamageProcedure.channel, battle, battle.storedDamageProcedure.author);
      this.endOfAction(battle);
    }
  }

  // CONTINUE WITH REDUCE HEALTH
  private continueReduceHealth(battle: Battle6D) {
    if (battle.storedDamageProcedure != null) {
      this.reduceHealth(battle.storedDamageProcedure.targetTank, battle.storedDamageProcedure.finalDamage, battle.channel, battle);
      this.endOfAction(battle);
    }
  }
}