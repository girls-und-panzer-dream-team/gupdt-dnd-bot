import { Battle6D } from "../model/battle";
import { TextChannel, GuildMember, Role, Message } from "discord.js";
import { tankNames, tankStats } from "./../tanklist.json";
import { Tank6D } from "./../model/tank";
import { BattleMiscFunctions6D } from "./battle-misc-functions";
import { BattleMessages6D } from "../messages/battle-messages";
import { BattleMain6D } from "../battle-main";

export class BattlePreparationFunctions6D {
    helper: BattleMiscFunctions6D = new BattleMiscFunctions6D();
    battleMessage: BattleMessages6D = new BattleMessages6D();

    // PREPARE A NEW BATTLE
    public prepareBattle(channel: TextChannel, author: GuildMember, battles: Battle6D[], message: Message): boolean {
        // CHECK IF THERE IS A BATTLE IN THAT CHANNEL ALREADY
        if (this.helper.findBattle(channel, battles) != null) {
          return false;
        }
        let newBattle = new Battle6D();
        newBattle.owner = author;
        newBattle.channel = channel;
        newBattle.message = message;
        battles.push(newBattle);
    
        return true;
      }
    
      // JOIN BATTLE AS COMMANDER
      public async joinBattle(channel: TextChannel, author: GuildMember, content: string, battles: Battle6D[]): Promise<string> {
        let battle = this.helper.findBattle(channel, battles);
    
        if (battle == null) {
          return `(There is currently no Battle being prepared!)`;
        }
        if (battle.started) {
          return `(The current Battle has already started!)`;
        }
        if (battle.participants.includes(author)) {
          return `(You have already joined this Battle!)`;
        }
        // clean String
        let cleanString = content.substring(13);
    
        // define Team
        let teamString = cleanString.substring(0, 1);
        if (teamString.toUpperCase() !== "A" && teamString.toUpperCase() !== "B") {
          return "(Please input A or B as team)";
        }
    
        let tankString = cleanString.substring(2);
        let tankList: string[] = [];
        tankList = tankList.concat(tankNames.nameListMedium);
        tankList = tankList.concat(tankNames.nameListLight);
        tankList = tankList.concat(tankNames.nameListHeavy);
        tankList = tankList.concat(tankNames.nameListTankDestroyer);
        tankList = tankList.concat(tankNames.nameListSuperHeavy);
        tankList = tankList.concat(tankNames.nameListMBT);

        let found = false;
        for(let i = 0; i < tankList.length; i++) {
          let regex = new RegExp (tankList[i], "i")
          if (regex.test(tankString)) {
            found = true;
            tankString = tankList[i];
            break;
          }
        }
        if (!found) {
          return `(Please choose a Tank from the Tanklist! \`!battle tanklist\`)`;
        }

        battle.participants.push(author);
        let newTank = new Tank6D();
        newTank.commander = author;
        newTank.commanderName = author.displayName;
        newTank.tankName = tankString;
        newTank.health = tankStats[tankString].health;
        newTank.hitThreshhold = tankStats[tankString].hitThreshold;
        newTank.special = tankStats[tankString].special;
        newTank.cooldown = 0;
        newTank.type = tankStats[tankString].type;
    
        if (teamString === "A" || teamString === "a") {
            battle.teamA.push(newTank)
        } else {
            battle.teamB.push(newTank)
        }
        return (`(${author.displayName} joined Team ${teamString} with a ${tankString} (HP: ${newTank.health} | HT: ${newTank.hitThreshhold})`);
      }
    
      // LEAVE BATTLE PREPERATION
      public leaveBattle(channel: TextChannel, author: GuildMember, battles: Battle6D[]): string {
        let battle = this.helper.findBattle(channel, battles);
    
        if (battle == null) {
          return `(There is currently no Battle for you to leave)`;
        }
        if (battle.started) {
          return `(The Battle has already started, use \`!battle surrender\` to leave early)`;
        }
        if (!battle.participants.includes(author)) {
          return `(You have not joined the Battle preparations)`;
        }
    
        this.helper.removeFromBattle(battle, author, null);
    
        return `(${author.displayName} has left the Battle preparations)`;
      }
    
      // CANCEL BATTLE
      public cancelBattle(channel: TextChannel, author: GuildMember, battles: Battle6D[]): string {
    
        let battle = this.helper.findBattle(channel, battles)
    
        if (battle == null) {
          return `(There is no battle to cancel)`;
        }
    
        let isAdmin = false;
        author.roles.cache.forEach((value: Role) => {
          if (value.name === "Admin") {
            isAdmin = true;
          }
        })
    
        if (battle.owner.id !== author.id && !isAdmin) {
          return `(You do not have permission to cancel the current Battle)`;
        }
    
        let index = battles.indexOf(battle);
        battles.splice(index, 1);
    
        return "(**-- BATTLE CANCELLED --**)"
      }
    
      // START BATTLE
      public startBattle(channel: TextChannel, author: GuildMember, battles: Battle6D[]): boolean {
        let battle = this.helper.findBattle(channel, battles);
    
        let isAdmin = false;
        author.roles.cache.forEach((value: Role) => {
          if (value.name === "Admin" || value.name === "Technician") {
            isAdmin = true;
          }
        })

        if (!isAdmin) {
          if (!battle.participants.includes(author)) {
            return false;
          }
        }
    
        battle.started = true;

        let i = 0;
        battle.teamA.forEach(tank => {
          tank.targetId = "A" + i;
          i++;
        });
        i = 0;
        battle.teamB.forEach(tank => {
          tank.targetId = "B" + i;
          i++;
        });

        battle.rosterTeamA = [];
        battle.rosterTeamB = [];
        battle.rosterTeamA = battle.rosterTeamA.concat(battle.teamA);
        battle.rosterTeamB = battle.rosterTeamB.concat(battle.teamB);
        
        this.battleMessage.displayTeams(battle);
        battle.channel.send(this.battleMessage.initiativePhase())
        return true;
      }
}