import { Battle6D } from "../model/battle";
import { TextChannel, GuildMember } from "discord.js";
import { tankStats } from "./../tanklist.json";
import { Tank6D } from "./../model/tank";
import { BattleDataFunctions6D } from "./battle-data-functions";
import { BattleMessages6D } from "../messages/battle-messages";

export class BattleMiscFunctions6D {
  // HELPER FOR SIMPLE DATA MAPPING
  private battleDataFunctions: BattleDataFunctions6D = new BattleDataFunctions6D();
  private battleMessage: BattleMessages6D = new BattleMessages6D();

  // FIND BATTLE BY CHANNEL
  public findBattle(channel: TextChannel, battles: Battle6D[]) {
    for (let i = 0; i < battles.length; i++) {
      if (battles[i].channel.id === channel.id) {
        return battles[i];
      }
    }
    return null;
  }

  // FIND TANK BY COMMANDER
  public findTank(battle: Battle6D, author: GuildMember): Tank6D {
    for (let i = 0; i < battle.teamA.length; i++) {
      if (battle.teamA[i].commander.id === author.id) {
        return battle.teamA[i];
      }
    }
    for (let i = 0; i < battle.teamB.length; i++) {
      if (battle.teamB[i].commander.id === author.id) {
        return battle.teamB[i];
      }
    }
    return null;
  } 

  // FIND TANK BY TARGET ID
  public findTankByTargetId(battle: Battle6D, targetId: string) {
    for (let i = 0; i < battle.teamA.length; i++) {
      if (battle.teamA[i].targetId === targetId) {
        return battle.teamA[i];
      }
    }
    for (let i = 0; i < battle.teamB.length; i++) {
      if (battle.teamB[i].targetId === targetId) {
        return battle.teamB[i];
      }
    }
    return null;
  }

  // REMOVE TANK FROM BATTLE
  public removeFromBattle(battle: Battle6D, author: GuildMember, tankToRemove: Tank6D) {
    const index = battle.participants.indexOf(author);
    battle.participants.splice(index, 1);
    
    let tank = tankToRemove == null ? this.findTank(battle, author) : tankToRemove;
    if (tank != null) {
      let index = battle.teamA.indexOf(tank)
      if (index != -1) {
        battle.teamA.splice(index, 1);
        return;
      }
      index = battle.teamB.indexOf(tank)
      if (index != -1) {
        battle.teamB.splice(index, 1);
        return;
      }
    }
  }

  // NOTIFY COMMANDER TO ACT
  public notifyCommander(tank: Tank6D, turnStart: boolean, battle: Battle6D) {
    if (turnStart) {
      if (tank.special.includes("DevilishSpeed") && tank.clearSpecial) {
        tank.hitThreshhold = tankStats[tank.tankName].hitThreshold;
      }
    }
    battle.channel.send(this.battleMessage.notifyCommander(tank.commander))
  }

  // REMOVE DESTROYED TANK FROM BATTLE
  public async tankDestroyed(battle: Battle6D, targetTank: Tank6D) {
    // REMOVE FROM BATTLE
    let index = battle.turnDone.indexOf(targetTank);
    if (index != -1) {
      battle.turnDone.splice(index, 1);
    }

    index = battle.turnOrder.indexOf(targetTank);
    if (index != -1) {
      battle.turnOrder.splice(index, 1);
    }
    this.removeFromBattle(battle, targetTank.commander, targetTank)
  }

  public checkForEndOfBattle(battle: Battle6D): string {
    if (battle.teamA.length <= 0) {
      return "Team B";
    } else if (battle.teamB.length <= 0) {
      return "Team A";
    }
    return null;
  }

  public checkEndTurn(battle: Battle6D) {
    if (battle.turnOrder.length === 0) {
      battle.turnOrder = battle.turnDone;
      battle.turnOrder.sort((a: Tank6D, b: Tank6D) => { return this.battleDataFunctions.sortByInitiative(a,b)})
      battle.turnDone = [];
      battle.round = battle.round + 1;
      battle.turnOrder.forEach(tank => {
        if (tank.cooldown > 0) {
          tank.cooldown = tank.cooldown -1;
        }
      })
    }
  }
}