
import { Tank6D } from "./../model/tank";

export class BattleDataFunctions6D {
  // ROLL DICE
  public rollDice(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  // TURN ORDER SORT
  public sortByInitiative(a: Tank6D, b: Tank6D): number {
    if (a.initative < b.initative) {
      return 1;
    }
    if (a.initative > b.initative) {
      return -1;
    }
    return 0;
  }

  // FIND HIT LOCATION 
  public findHitLocation(roll: number): string {
    if (roll <= 3) {
      return "hull";
    } else if (roll <= 5) {
      return "turret";
    } else if (roll <= 7) {
      return "tracks";
    } else if (roll <= 9) {
      return "side";
    }
    return "weakspot";
  }

  public checkLocationDamage(hitLocation: string, targetTank: Tank6D, firingTank: Tank6D): number {
    // EXPAND BY TANK RULES LATER
    switch(hitLocation) {
      case "hull":
      case "turret":
      case "tracks":
        return 0;
      case "side":
        if (firingTank.special.includes("Sidesweep")) {
          return 1.5;
        }
        return 1;
      case "weakspot":
        return 0;
    }
  }

  // DETERMINE DAMAGE
  public findDamagePointValue(damageRoll: number) {
    if (damageRoll <= 25) {
      return 0.5;
    } else if (damageRoll <= 74) {
      return 1;
    } else if (damageRoll <= 89) {
      return 2;
    } else if (damageRoll <= 99) {
      return 3;
    } else if (damageRoll >= 100) {
      return 4;
    }
  }

  findDamageStringValue(damagePointsFinal: number): string {
    if (damagePointsFinal <= 0.5) {
      return "grazed";
    } else if (damagePointsFinal <= 1) {
      return "lightly damaged"
    } else if (damagePointsFinal <= 2) {
      return "damaged";
    } else if (damagePointsFinal <= 3) {
      return "heavily damaged";
    } else if (damagePointsFinal >= 4) {
      return "critically damaged";
    }
  }
}
