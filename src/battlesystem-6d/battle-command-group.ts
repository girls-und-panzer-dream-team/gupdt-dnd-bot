import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { BattlePrepareCommand6D } from "./commands/battle-prepare-command"
import { BattleJoinCommand6D } from "./commands/battle-join-command"
import { BattleTanklistCommand6D } from "./commands/battle-tanklist-command"
import { BattleLeaveCommand6D } from "./commands/battle-leave-command"
import { BattleStartCommand6D } from "./commands/battle-start-command"
import { BattleCancelCommand6D } from "./commands/battle-cancel-command"
import { BattleInitiativeCommand6D } from "./commands/battle-initiative-command"
import { BattleTargetCommand6D } from "./commands/battle-target-command"
import { BattleHelpCommand6D } from "./commands/battle-help-command"
import { BattleCreditsCommand6D } from "./commands/battle-credits-command"
import { BattleSurrenderCommand6D } from "./commands/battle-surrender-command"
import { BattleStatusCommand6D } from "./commands/battle-status-command"
import { BattleConfirmCommand6D } from "./commands/battle-confirm-command"
import { BattleDenyCommand6D } from "./commands/battle-deny-command"
import { BattleSkipCommand6D } from "./commands/battle-skip-command"
import { BattleSpecialCommand6D } from "./commands/battle-special-command"

export const BattleCommandGroup6D = new CommandGroup()
  .match(matchPrefixes("!battle"))
  .setCommands(BattlePrepareCommand6D, BattleJoinCommand6D, BattleTanklistCommand6D, BattleLeaveCommand6D, BattleStartCommand6D, BattleCancelCommand6D,
    BattleInitiativeCommand6D, BattleTargetCommand6D, BattleHelpCommand6D, BattleCreditsCommand6D, BattleSurrenderCommand6D, BattleStatusCommand6D,
    BattleConfirmCommand6D, BattleDenyCommand6D, BattleSkipCommand6D, BattleSpecialCommand6D)