import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";

export const BattleLeaveCommand6D = new Command()
  .match(matchPrefixes("leave"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);

    if (service.channels.includes(message.channel.id)){
      let leaveSuccess = service.leaveBattle(message.channel as TextChannel, message.member);

      return message.channel.send(leaveSuccess);
    }
  });
