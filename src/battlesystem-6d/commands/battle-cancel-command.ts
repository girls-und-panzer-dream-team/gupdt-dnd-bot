import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";

export const BattleCancelCommand6D = new Command()
  .match(matchPrefixes("cancel"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);

    let cancelValid = service.cancelBattle(message.channel as TextChannel, message.member);

    return message.channel.send(cancelValid);
  });
