import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { BattleMain6D } from "../battle-main";

export const BattleCreditsCommand6D = new Command()
  .match(matchPrefixes("credits"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);

    if (service.channels.includes(message.channel.id)){
      return message.channel.send("**-- CREDITS --**\n" +
          "- Original Rules System: **6D Chess Master**\n" +
          "- Rules refinement: **Saki Maruyama**\n" +
          "- Discord-Bot Battle System: **Mika**\n" +
          "- Testers:\n" +
          "  - **Saori Takebe**\n" +
          "  - **Rena Andou**\n" +
          "  - **Erika Itsumi**\n" +
          "- Frameworks used:\n" +
          "  - **@enitoni/gears-discordjs**"
      );
    }
  });
