import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";
import { BattleMain } from "../../battlesystem/battle-main";

export const BattlePrepareCommand6D = new Command()
  .match(matchPrefixes("prepare"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);
    const serviceAdv = manager.getService(BattleMain);

    if (service.channels.includes(message.channel.id)){
      if (!serviceAdv.noBattleActive(message.channel as TextChannel)) {
        return message.channel.send("(**THERE IS AN ADVANCED BATTLE BEING FOUGHT IN THIS CHANNEL ALREADY**)")
      }

      let battleValid = service.prepareBattle(message.channel as TextChannel, message.member, message);

      if (battleValid) {
        return message.channel.send(
          "*Welcome to 6D Chess Master's CLASSIC GuPDT RP Battle System*\n" +
          "**-- PREPARING BATTLE --**\n" + 
          "Participants join with `!battle join [team] [tank]` (Team: A, B)\n" +
          "To see a list of available Tanks use `!battle tanklist`\n" +
          "To see a list of commands use `!battle help`"
        );
      }
      return message.channel.send(
        "(**THERE IS AN UNRESOLVED BATTLE GOING ON IN THIS CHANNEL**)"
      );
    }
  });
