import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";

export const BattleSpecialCommand6D = new Command()
  .match(matchPrefixes("special"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);

    service.special( message.channel as TextChannel, message.member, null);
  });
