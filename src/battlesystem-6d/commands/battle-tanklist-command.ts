import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { tankNames } from "./../tanklist.json";
import { BattleMain6D } from "../battle-main";

export const BattleTanklistCommand6D = new Command()
  .match(matchPrefixes("tanklist"))
  .use(context => {
    const { manager, message } = context
    const service = manager.getService(BattleMain6D);

    if (service.channels.includes(message.channel.id)){
    return message.channel.send(   "Light Tanks: `" + tankNames.nameListLight.toString() + "`\n" +
        "Medium Tanks: `" + tankNames.nameListMedium.toString() + "`\n" +
        "Heavy Tanks: `" + tankNames.nameListHeavy.toString() + "`\n" +
        "Tank Destroyers: `" + tankNames.nameListTankDestroyer.toString() + "`\n" + 
        "Super Heavies: `" + tankNames.nameListSuperHeavy.toString() + "`\n" +
        "MBTs: `" + tankNames.nameListMBT.toString() + "`");
    }
  })