import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";

export const BattleJoinCommand6D = new Command()
  .match(matchPrefixes("join"))
  .use(async (context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);

    if (service.channels.includes(message.channel.id)){
      let joinSuccess = await service.joinBattle(message.channel as TextChannel, message.member, message.content, message);

      if (service.canStartBattle(message.channel as TextChannel)) {
        return message.channel.send(joinSuccess + "\n(Both Teams have at least one Tank use `!battle start` to begin!)");
      }
      return message.channel.send(joinSuccess);
    }
  });
