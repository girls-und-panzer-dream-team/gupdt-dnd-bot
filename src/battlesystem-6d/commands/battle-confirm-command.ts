import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";

export const BattleConfirmCommand6D = new Command()
  .match(matchPrefixes("confirm"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);
    service.confirm(message.channel as TextChannel, message.member);
  });
