import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain6D } from "../battle-main";

export const BattleSurrenderCommand6D = new Command()
  .match(matchPrefixes("surrender"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain6D);

    service.surrender(message.channel as TextChannel, message.member);
  });
