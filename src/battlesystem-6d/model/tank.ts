import { GuildMember } from "discord.js";

export class Tank6D {
    commander: GuildMember;
    commanderName: string;
    tankName: string;
    tracked: boolean;
    health: number;
    hitThreshhold: number;
    initative: number;
    targetId: string;
    special: string[];
    cooldown: number = 0;
    specialUsed: boolean;
    clearSpecial: boolean;
    type: string;
}