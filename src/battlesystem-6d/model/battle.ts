import { TextChannel, GuildMember, Message } from "discord.js";
import { Tank6D } from "./tank";
import { StoredDamageProcedure6D } from "./special/stored-damage-procedure";

export class Battle6D {
    channel: TextChannel;
    owner: GuildMember;
    teamA: Tank6D[] = [];
    teamB: Tank6D[] = [];
    round: number = 0;
    started: boolean;
    turnOrder: Tank6D[] = [];
    turnDone: Tank6D[] = [];
    participants: GuildMember[] = [];
    confirmant: Tank6D;
    storedDamageProcedure: StoredDamageProcedure6D;
    message: Message;
    rosterTeamA: Tank6D[] = [];
    rosterTeamB: Tank6D[] = [];
}