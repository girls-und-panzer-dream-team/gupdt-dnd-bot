import { Tank6D } from "./../../model/tank";
import { TextChannel, GuildMember } from "discord.js";

export class StoredDamageProcedure6D {
    specialRuleApplied: string;
    targetTank: Tank6D;
    firingTank: Tank6D;
    hitLocation: string;
    channel: TextChannel;
    author: GuildMember;
    damageroll: number;
    locationDamage: number;
    finalDamage: number;
    entrypoint: string;
}