import { Battle6D } from "../model/battle";
import { GuildMember } from "discord.js";
import { Tank6D } from "./../model/tank";

export class BattleMessages6D {
    /********************************************************/
    /*                 INITIATIVE MESSAGES                  */
    /********************************************************/
    // START INITIATIVE PHASE MESSAGE
    public initiativePhase(): string {
        return `(Use \`!battle initiative\` to determine turn-order.)`
    }
    // INITIATIVE ROLL MESSAGE
    public initiativeRoll(commanderName: string, initiativeRoll: number): string {
        return `(${commanderName} tests for initiative \`1d10\` -roll:\`${initiativeRoll}\`)`
    }
    public initiativeWarn(author: GuildMember): string {
        return `(You already have rolled for initiative ${author.displayName})`
    }
    // DISPLAYED AFTER INITIATIVE PHASE
    public openBattleMessage(battle: Battle6D) {
        let turnOrderString = `TURN ORDER:\n`;
        battle.turnOrder.forEach(tank => {
        turnOrderString = `${turnOrderString}${tank.initative}: ${tank.commanderName} (${tank.tankName}) Target-ID: ${tank.targetId}\n`; 
        })

        battle.channel.send(`${turnOrderString}\n(Use \`!battle target [target-id] [location](location: hull, turret, tracks, side, weakspot (location is optional: 'Aiming')\`)`);
    }

    /********************************************************/
    /*                   BATTLE MESSAGES                    */
    /********************************************************/
    // --- TARGETING ---
    // WARN FOR INVALID TARGET
    public invalidTargetWarn(args: string[]): string {
        return `(${args[0]} is not a valid Target! use \`!battle status\` for a list of targets!)`
    }

    // WARN FOR SHOOTING SELF
    public suicideWarn(author: GuildMember): string {
        return `(You can't shoot yourself ${author.displayName}!)`
    }

    // --- AIMING ---
    // WARN FOR INVALID AIM TARGET
    public invalidAimWarn(): string {
        return `(Invalid target for aiming! Target must be one of: \`hull, turret, tracks, side, weakspot\`)`;
    }
    // WARN CANT AIM WHILE TRACKED
    public cantAimCauseTracked(): string {
        return `(Your Tank is tracked and unable to position itself to aim!)`;
    }

    // --- HITCHECKS ---
    // HIT RESULT MESSAGE
    public hitCheckMessage(commanderName: string, targetTank: Tank6D, isAiming: string, aimingTarget: number, hitRoll: number, hitResult: string): string {
        return `*${commanderName} fires at ${ targetTank.commanderName}'s ${targetTank.tankName} ${isAiming} and ${hitResult}* (\`1d10\` -target: \`${aimingTarget}\` -roll: \`${hitRoll}\`)`
    }

    // AIMED HIT LOCATION MESSAGE
    public hitAimLocationMessage(commanderName: string, targetTank: Tank6D, hitLocation: string): string {
        return `*${commanderName} aimed and hit ${targetTank.commanderName}'s ${targetTank.tankName}'s ${hitLocation}*`;
    }

    // UNAIMED HIT LOCATION MESSAGE
    public hitRandomLocationMessage(commanderName: string, targetTank: Tank6D, hitLocation: string, hitLocationRoll: number): string {
        return `*${commanderName}'s shell hit ${targetTank.commanderName}'s ${targetTank.tankName} in the* \`${hitLocation}\`(\`1d10\` -roll: \`${hitLocationRoll}\`)`;
    }

    // --- DAMAGE ---
    // TRACK DAMAGE MESSAGE
    public trackHitMessage(targetTank: Tank6D): string {
        return `*${targetTank.commanderName}'s ${targetTank.tankName} tracks were destroyed* (HT: \`${targetTank.hitThreshhold}\`)`;
    }

    // GENERAL DAMAGE MESSAGE
    public damageMessage(commanderName: string, damageString, damageRoll: number, damagePoints: number, locationDamage: number, damageMultiplier: number, damagePointsFinal: number): string {
        return `*${commanderName}'s shell ${damageString} the target* (\`1d100\` -roll: \`${damageRoll}\` -(damage: \`${damagePoints}\` location damage: \`${locationDamage}\`) x Damage multiplier: \`${damageMultiplier}\` = \`${damagePointsFinal}pts\`)`
    }

    // HP REMAINING MESSAGE
    public healthRemainingMessage(targetTank: Tank6D, damageString: string, damagePointsFinal: number): string {
        return `*${targetTank.commanderName}'s ${targetTank.tankName} is ${damageString}* (HP: \`${targetTank.health}\` - \`${damagePointsFinal}\` = \`${targetTank.health-damagePointsFinal}pts\`)`
    }

    // --- MISC ---
    // TANK SURRENDER
    public surrender(tank: Tank6D): string {
        return `*${tank.commanderName} has surrendered and the ${tank.tankName} ejects the* :flag_white:`;
    }

    // TANK DESTROYED
    public tankDestroyedMessage(targetTank: Tank6D): string {
        return `*${targetTank.commanderName}'s ${targetTank.tankName} has been disabled and ejects the* :flag_white:`
    }

    // ANNOUNCE WINNER
    public announceWinner(looser: string, winner: string): string {
        return `(**All tanks of ${looser} have surrendered or been disabled - ${winner} wins the match!**)`
    }

    /********************************************************/
    /*                   COMMON MESSAGES                    */
    /********************************************************/
    // NOTIFY COMMANDER TO ACT
    public notifyCommander(commander: GuildMember): string {
        return `(${commander} it is your turn to act!)`
    }

    // WARN ABOUT TURN ORDER
    public notYourTurn(author: GuildMember): string {
        return `(It is not your turn to act ${author.displayName}!)`
    }

    // DISPLAY TEAMS
    public displayTeams(battle: Battle6D) {
        let teamAList = `**Team A:**\n`;
        let teamBList = `**Team B:**\n`;

        battle.rosterTeamA.forEach(tank => {
            let status = tank.health > 0 ? `:white_check_mark:` : `:flag_white:`;
            let tracked = tank.tracked ? ` | Track damaged`: ``;
            teamAList = `${teamAList}${status} ${tank.commanderName} : ${tank.tankName} (HP: ${tank.health} | HT: ${tank.hitThreshhold + tracked}) - Target-ID: **${tank.targetId}**\n`
        });
        battle.channel.send(`${teamAList}\n`)
        battle.rosterTeamB.forEach(tank => {
            let status = tank.health > 0 ? `:white_check_mark:` : `:flag_white:`;
            let tracked = tank.tracked ? ` | Track damaged`: ``;
            teamBList = `${teamBList}${status} ${tank.commanderName} : ${tank.tankName} (HP: ${tank.health} | HT: ${tank.hitThreshhold + tracked}) - Target-ID: **${tank.targetId}**\n`
        })
        battle.channel.send(`${teamBList}`)
    }
}