import { GuildMember } from "discord.js";

export class Tank {
    commander: GuildMember;
    commanderName: string;
    isAiCommander: boolean;
    tankName: string;
    tracked: boolean;
    health: number;
    hitThreshhold: number;
    initative: number;
    targetId: string;
    special: string[];
    cooldown: number = 0;
    specialUsed: boolean;
    clearSpecial: boolean;
    type: string;
    crewxp: number;
    xpTransfered: boolean;
    misses: number;
    damageTotal: number;
    damageHull: number;
    damageTurret: number;
    damageSide: number;
    damageWeakspot: number;
    position: string;
}