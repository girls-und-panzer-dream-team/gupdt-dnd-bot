import { TextChannel, GuildMember, Message } from "discord.js";
import { Tank } from "./tank";
import { StoredDamageProcedure } from "./special/stored-damage-procedure";

export class Battle {
    channel: TextChannel;
    owner: GuildMember;
    teamA: Tank[] = [];
    teamB: Tank[] = [];
    round: number = 0;
    started: boolean;
    turnOrder: Tank[] = [];
    turnDone: Tank[] = [];
    participants: GuildMember[] = [];
    confirmant: Tank;
    storedDamageProcedure: StoredDamageProcedure;
    message: Message;
    rosterTeamA: Tank[] = [];
    rosterTeamB: Tank[] = [];
}