import { Tank } from "./../../model/tank";
import { TextChannel, GuildMember } from "discord.js";

export class StoredDamageProcedure {
    specialRuleApplied: string;
    targetTank: Tank;
    firingTank: Tank;
    hitLocation: string;
    channel: TextChannel;
    author: GuildMember;
    damageroll: number;
    locationDamage: number;
    finalDamage: number;
    entrypoint: string;
}