import { Battle } from "../model/battle";
import { GuildMember } from "discord.js";
import { Tank } from "./../model/tank";

export class SpecialMessages {
    /********************************************************/
    /*                SPECIAL RULES MESSAGES                */
    /********************************************************/
    // SPECIAL ALREADY USED
    public specialUsed(author: GuildMember): string {
        return `(You have already used your special this match, ${author.displayName}!)`
    }
    // SPECIAL COOLDOWN
    public cooldownWarning(author: GuildMember, tank: Tank): string {
        return `(Your special is on cooldown for ${tank.cooldown} more turns, ${author.displayName}!)`
    }

    // SPECIAL REROLL
    public rerollHitMessage(commanderName: string, targetTank: Tank, isAiming: string, aimingTarget: number, hitRoll: number, specialRuleString: string): string {
        return `*${commanderName} fires at ${ targetTank.commanderName}'s ${targetTank.tankName} ${isAiming} and misses* (\`1d10\` -target: \`${aimingTarget}\` -roll: \`${hitRoll}\`)\n (**SPECIAL RULE: ${specialRuleString}** - free reroll to hit`
    }

    // SPECIAL RULE: I AM SPEED
    public specialTrackHitMessage(targetTank: Tank): string {
        return `*${targetTank.commanderName}'s ${targetTank.tankName} tracks were destroyed but it keeps going!* (**SPECIAL RULE: I AM SPEED** - HT: \`${targetTank.hitThreshhold}\`)`
    }

    // SPECIAL RULE: DEVILISH SPEED
    public devilishSpeedActivate(author: GuildMember): string {
        return `(${author.displayName} uses **SPECIAL RULE: DEVILISH SPEED** - increase Hit Threshold to 10 for one turn)`
    }

    // SPECIAL RULE: MASS PRODUCED
    public massProducedWarn(author: GuildMember): string {
        return `(Your Tank already is at max Health, ${author.displayName}!`
    }
    public massProducedActivate(author: GuildMember): string {
        return `(${author.displayName} uses **SPECIAL RULE: MASS PRODUCED** - heal 1HP)`;
    }

    // SPECIAL RULE: DUCK POWER
    public duckPowerActivate(author: GuildMember): string {
        return `(${author.displayName} uses **SPECIAL RULE: DUCK POWER** - sacrifice ability to aim to increase Hit Threshold)`;
    }
    public cantAimDuckPower(): string {
        return `(Your Tank sacrificed it's ability to aim with **SPECIAL RULE: DUCK POWER!**)`;
    }

    // SPECIAL RULE: STEADY
    public steadyActivate(author: GuildMember): string {
        return `(${author.displayName} uses **SPECIAL RULE: STEADY...** - sacrifice turn for a focused shot +2 to hit roll)`
    }
    public steadyExecute(author: GuildMember, hitRoll: number): string {
        return `(**SPECIAL RULE: STEADY... ** ${author.displayName} modified hitroll: \`${hitRoll-2}\` to: \`${hitRoll}\`)`
    }

    // SPECIAL RULE: SPACED STEEL
    public spacedSteelActivate(author: GuildMember): string {
        return `(${author.displayName} uses **SPECIAL RULE: SPACED STEEL** - sacrifice ability to aim to increase health by 1.5)`
    }

    // SPECIAL RULE: THIS IS WHERE THE FUN BEGINS
    public thisIsWhereTheFunBeginsMessage(bonus: number): string {
        return bonus === 10 ? `(**SPECIAL RULE: THIS IS WHERE THE FUN BEGINS:** +10 initiative)` : `(**SPECIAL RULE: SANDSWEPT HORIZON:** +2 initiative)`
    }

    // SPECIAL RULE: SLOW AND STEADY
    public cantAimSlowButSure(): string {
        return "(The Tiger II H is incapable of aiming SPECIAL RULE: SLOW BUT SURE)"
    }

    // SPECIAL RULE: SIZE MATTERS
    public sizeMattersExecute(): string {
        return `(**SPECIAL RULE: SIZE MATTERS: ** tanks with base health of 5 or less are instantly knocked out)`
    }

    // SPECIAL RULE: SHOW ME WHERE IT HURTS
    public showMeWhereItHurtsQuestion(firingTank: Tank): string {
        return `(**SPECIAL RULE: SHOW ME WHERE IT HURTS** ${firingTank.commander} reroll hit location? \`?battle confirm\` / \`?battle deny\`)`
    }
    public showMeWhereItHurtsExecute(battle: Battle, hitLocationRoll: number): string {
        return `*${battle.storedDamageProcedure.targetTank.commanderName}'s ${battle.storedDamageProcedure.targetTank.tankName} was hit in the ${battle.storedDamageProcedure.hitLocation}* (**SPECIAL RULE: SHOW ME WHERE IT HURTS** hit location reroll \`1d10\` -roll: \`${hitLocationRoll}\`)`
    }

    // SPECIAL RULE: LUCK OF THE FOOL
    public luckOfTheFool(avoidRoll: number, hitLocation: string): string {
        return `(**SPECIAL RULE: LUCK OF THE FOOL** transfer hit from \`weakspot\` to \`hull\` \`1d100\` -target: \`60\` - roll: \`${avoidRoll}\` -hitlocation: \`${hitLocation}\`)`
    }

    // SPECIAL RULE: OL RELIABLE
    public olReliableQuestion(targetTank: Tank): string {
        return `(**SPECIAL RULE: OL' RELIABLE** ${targetTank.commander} transfer hit from \`weakspot\` to \`tracks\`? \`?battle confirm\` / \`?battle deny\`)`
    }

    // SPECIAL RULE: SPACING IS KEY
    public spacingIsKeyExecute(): string {
        return `(**SPECIAL RULE: SPACING IS KEY** - +20 to damage roll)`;
    }
    public spacingIsKeyExecute2() {
        return `(**SPECIAL RULE: SPACING IS KEY** - final damage to hull halved)` 
    }
    
    // SPECIAL RULE: TRAFALAGER'S PRIDE
    public trafalagersPrideExecute(): string {
        return `(**SPECIAL RULE: TRAFALAGER'S PRIDE** - +15 to damage roll when fighting Light Tanks)`;
    }   
    
    // SPECIAL RULE: PUMP YOU UP
    public pumpYouUpExecute(): string {
        return `(**SPECIAL RULE: PUMP YOU UP** - +15 to all damage rolls)`;
    }
    
    // SPECIAL RULE: FRENCH COMPETANCE
    public frenchCompetanceExecute(): string {
        return `(**SPECIAL RULE: FRENCH COMPETANCE** - -5 to damage roll)`;
    }
    
    // SPECIAL RULE: A DASH OF OOMPH
    public aDashOfOomphExecute(): string {
        return `(**SPECIAL RULE: A DASH OF OOMPH** - +5 to damage roll)`;
    }
    
    // SPECIAL RULE: DOWNUNDER POINT OF VIEW
    public downUnderPointOfViewQuestion(firingTank: Tank, damageRoll: number, reverseNumber: number): string {
        return `(**SPECIAL RULE: DOWN UNDER POINT OF VIEW** ${firingTank.commander} switch damage roll from \`${damageRoll}\` to \`${reverseNumber}\`? \`?battle confirm\` / \`?battle deny\`)`
    }

    // SPECIAL RULE: PATRIOTIC PUNCH
    public patrioticPunchQuestion(firingTank: Tank, damageRoll: number): string {
        return `(**SPECIAL RULE: PATRIOTIC PUNCH** ${firingTank.commander} switch damage roll from \`${damageRoll}\` to \`76\`? \`?battle confirm\` / \`?battle deny\`)`
    }

    // SPECIAL RULE: MEMORIES OF RASEINIAI
    public memoriesOfRaseiniaiQuestion(firingTank: Tank, damageRoll: number): string {
        return `(**SPECIAL RULE: MEMORIES OF RASEINIAI** ${firingTank.commander} reroll damage roll of \`${damageRoll}\`? \`?battle confirm\` / \`?battle deny\`)`
    }

    // SPECIAL RULE: STRENGTH OF LEGIONS
    public strengthOfLegionsQuestion(targetTank: Tank, damageRoll: number): string {
        return `(**SPECIAL RULE:STRENGTH OF LEGIONS** ${targetTank.commander} make enemy reroll damage roll of \`${damageRoll}\`? \`?battle confirm\` / \`?battle deny\`)`
    }
    public strengthOfLegionsExecute(battle: Battle): string {
        return `(**SPECIAL RULE:STRENGTH OF LEGIONS** ${battle.storedDamageProcedure.firingTank.commanderName} rerolls for damage (\`1D100 \`) -roll: \`${battle.storedDamageProcedure.damageroll}\`)`
    }

    // SPECIAL RULE: A THOUSAND CUTS
    public aThousandCutsExecute(): string {
        return `(**SPECIAL RULE: A THOUSAND CUTS** - unmodified damage max 2)`
    }

    // SPECIAL RULE: TERRIBLE ROUNDS
    public terribleRoundsExecute(): string {
        return `(**SPECIAL RULE: TERRIBLE ROUNDS** - unmodified damage max 1.5 to tanks medium or higher)`
    }

    // SPECIAL RULE: A LITTLE OFF THE TOP
    public aLittleOffTheTopExecute(): string {
        return `(**SPECIAL RULE: A LITTLE OFF THE TOP** - shots to the turret 1.5x damage)`
    }

    // SPECIAL RULE: DAKKA GUNS
    public dakkaGunsExecute(): string {
        return `(**SPECIAL RULE: DAKKA GUNS** - only 0.5pts of damage to turret/hull/tracks)`
    }

    // SPECIAL RULE: BASED TANKING
    public basedTankingExecute(): string {
        return `(**SPECIAL RULE: BASED TANKING** - only 1pts of damage to turret/hull/tracks)`
    }

    // SPECIAL RULE: BIG BEASTS CLAW
    public bigBeastsClawExecute(): string {
        return `(**SPECIAL RULE: BIG BEASTS CLAW** - +1 to final damage)`
    }

    // SPECIAL RULE: PAPER WALLS
    public paperWallsExecuteHull(): string {
        return `(**SPECIAL RULE: PAPER WALLS** - damage to the hull do 1.5x damage)`
    }
    public paperWallsExecuteSide(): string {
        return `(**SPECIAL RULE: PAPER WALLS** - damage to side or weakspot do no damage)`
    }

    // SPECIAL RULE: THE RIGHT PLACES
    public theRightPlacesExecuteTurret(): string {
        return `(**SPECIAL RULE: THE RIGHT PLACES** - damage to the hull or turret do half damage)`
    }
    public theRightPlacesExecuteSide(): string {
        return `(**SPECIAL RULE: THE RIGHT PLACES** - damage to side or weakspot do double damage)`
    }

    // SPECIAL RULE: RUSSIAN BIAS
    public russianBiasQuestion(targetTank: Tank): string {
        return `(**SPECIAL RULE: RUSSIAN BIAS** ${targetTank.commander} ignore all Damage? \`?battle confirm\` / \`?battle deny\`)`
    }

    // SPECIAL RULE: PLOT ARMOR
    public plotArmorExecute(result: string, safeRoll: number): string {
        return `(**SPECIAL RULE: PLOT ARMOR** trying to avoid being disabled - ${result} (\`1d100\` -target:\`70\` -roll:\`${safeRoll}\`)`
    }

    // SPECIAL RULE: DESERT SNAKES
    public desertSnakesQuestion(tank: Tank): string {
        return `(**SPECIAL RULE: DESERT SNAKES** ${tank.commander} Shoot again at cost of 1.5HP? \`?battle confirm\` / \`?battle deny\`)`
    }
    public desertSnakesExecute(battle: Battle): string {
        return `(**SPECIAL RULE: DESERT SNAKES** ${battle.storedDamageProcedure.firingTank.commanderName}'s ${battle.storedDamageProcedure.firingTank.tankName} takes 1.5 pts of Damage to fire again: new Health: \´${battle.storedDamageProcedure.firingTank.health}\`)`;
    }
}