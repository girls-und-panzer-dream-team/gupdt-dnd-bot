import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { BattlePrepareCommand } from "./commands/battle-prepare-command"
import { BattleJoinCommand } from "./commands/battle-join-command"
import { BattleTanklistCommand } from "./commands/battle-tanklist-command"
import { BattleLeaveCommand } from "./commands/battle-leave-command"
import { BattleStartCommand } from "./commands/battle-start-command"
import { BattleCancelCommand } from "./commands/battle-cancel-command"
import { BattleInitiativeCommand } from "./commands/battle-initiative-command"
import { BattleTargetCommand } from "./commands/battle-target-command"
import { BattleHelpCommand } from "./commands/battle-help-command"
import { BattleCreditsCommand } from "./commands/battle-credits-command"
import { BattleSurrenderCommand } from "./commands/battle-surrender-command"
import { BattleStatusCommand } from "./commands/battle-status-command"
import { BattleConfirmCommand } from "./commands/battle-confirm-command"
import { BattleDenyCommand } from "./commands/battle-deny-command"
import { BattleSkipCommand } from "./commands/battle-skip-command"
import { BattleSpecialCommand } from "./commands/battle-special-command"
import { BattleUserCommand } from "./commands/battle-user-command"
import { BattleAddAICommand } from "./commands/battle-addai-command"
import { BattleAdvancedCommand } from "./commands/battle-advanced-command"
import { BattlePositionCommand } from "./commands/battle-position-command"

export const BattleCommandGroup = new CommandGroup()
  .match(matchPrefixes("?battle"))
  .setCommands(BattlePrepareCommand, BattleJoinCommand, BattleTanklistCommand, BattleLeaveCommand, BattleStartCommand, BattleCancelCommand,
    BattleInitiativeCommand, BattleTargetCommand, BattleHelpCommand, BattleCreditsCommand, BattleSurrenderCommand, BattleStatusCommand,
    BattleConfirmCommand, BattleDenyCommand, BattleSkipCommand, BattleSpecialCommand, BattleUserCommand, BattleAddAICommand, BattleAdvancedCommand, BattlePositionCommand)