import { Battle } from "../model/battle";
import { TextChannel, GuildMember } from "discord.js";
import { tankStats } from "./../tanklist.json";
import { Tank } from "./../model/tank";
import { BattleDataFunctions } from "./battle-data-functions";
import { BattleMessages } from "../messages/battle-messages";
import { GuPDTUserDao } from "../../database/dao/gupdt-user-dao";
import { BattleMain } from "../battle-main";
import { BattleAIFunctions } from "./battle-ai-functions";
import { GuPDTWalletDao } from "../../database/dao/gupdt-wallet-dao";
import { WalletHelper } from "../../walletsystem/helpers/wallet-helper";

export class BattleMiscFunctions {
  // HELPER FOR SIMPLE DATA MAPPING
  private battleDataFunctions: BattleDataFunctions = new BattleDataFunctions();
  private battleMessage: BattleMessages = new BattleMessages();
  private battleAIFunctions: BattleAIFunctions = new BattleAIFunctions();

  // FIND BATTLE BY CHANNEL
  public findBattle(channel: TextChannel, battles: Battle[]) {
    for (let i = 0; i < battles.length; i++) {
      if (battles[i].channel.id === channel.id) {
        return battles[i];
      }
    }
    return null;
  }

  // FIND TANK BY COMMANDER
  public findTank(battle: Battle, author: GuildMember): Tank {
    for (let i = 0; i < battle.teamA.length; i++) {
      if (battle.teamA[i].commander.id === author.id) {
        return battle.teamA[i];
      }
    }
    for (let i = 0; i < battle.teamB.length; i++) {
      if (battle.teamB[i].commander.id === author.id) {
        return battle.teamB[i];
      }
    }
    return null;
  } 

  // FIND TANK BY TARGET ID
  public findTankByTargetId(battle: Battle, targetId: string) {
    for (let i = 0; i < battle.teamA.length; i++) {
      if (battle.teamA[i].targetId === targetId) {
        return battle.teamA[i];
      }
    }
    for (let i = 0; i < battle.teamB.length; i++) {
      if (battle.teamB[i].targetId === targetId) {
        return battle.teamB[i];
      }
    }
    return null;
  }

  // REMOVE TANK FROM BATTLE
  public removeFromBattle(battle: Battle, author: GuildMember, tankToRemove: Tank) {
    const index = battle.participants.indexOf(author);
    battle.participants.splice(index, 1);
    
    let tank = tankToRemove == null ? this.findTank(battle, author) : tankToRemove;
    if (tank != null) {
      let index = battle.teamA.indexOf(tank)
      if (index != -1) {
        battle.teamA.splice(index, 1);
        return;
      }
      index = battle.teamB.indexOf(tank)
      if (index != -1) {
        battle.teamB.splice(index, 1);
        return;
      }
    }
  }

  // NOTIFY COMMANDER TO ACT
  public notifyCommander(tank: Tank, turnStart: boolean, battle: Battle, service: BattleMain) {
    if (turnStart) {
      if (tank.special.includes("DevilishSpeed") && tank.clearSpecial) {
        tank.hitThreshhold = tankStats[tank.tankName].hitThreshold;
      }
    }
    if (tank.isAiCommander) {
      battle.channel.send(`(It is the AI's turn to act!)`)
      this.battleAIFunctions.startTurnAct(tank, battle, service);
    } else {
      battle.channel.send(this.battleMessage.notifyCommander(tank.commander))
    }
  }

  // REMOVE DESTROYED TANK FROM BATTLE
  public async tankDestroyed(battle: Battle, targetTank: Tank) {
    // REMOVE FROM BATTLE
    let index = battle.turnDone.indexOf(targetTank);
    if (index != -1) {
      battle.turnDone.splice(index, 1);
    }

    index = battle.turnOrder.indexOf(targetTank);
    if (index != -1) {
      battle.turnOrder.splice(index, 1);
    }
    this.removeFromBattle(battle, targetTank.commander, targetTank)

    // APPLY XP APPLY LOSS
    if (!targetTank.isAiCommander) {
      let gupdtUserDao: GuPDTUserDao  = new GuPDTUserDao()
      let user = await gupdtUserDao.getUser(targetTank.commander.id, battle.message.guild.id);
      user.crewXp = targetTank.crewxp;
      user.battlesLost = user.battlesLost + 1;
      gupdtUserDao.addUser(user);
    }
    targetTank.xpTransfered = true;
  }

  public checkForEndOfBattle(battle: Battle): string {
    if (battle.teamA.length <= 0) {
      return "Team B";
    } else if (battle.teamB.length <= 0) {
      return "Team A";
    }
    return null;
  }

  public checkEndTurn(battle: Battle) {
    if (battle.turnOrder.length === 0) {
      battle.turnOrder = battle.turnDone;
      battle.turnOrder.sort((a: Tank, b: Tank) => { return this.battleDataFunctions.sortByInitiative(a,b)})
      battle.turnDone = [];
      battle.round = battle.round + 1;
      battle.turnOrder.forEach(tank => {
        tank.crewxp = tank.crewxp + 0.1;
        if (tank.cooldown > 0) {
          tank.cooldown = tank.cooldown -1;
        }
      })
    }
  }

  public recordDamage(damagePointsFinal: number, location: string, tank: Tank) {
    tank.damageTotal = tank.damageTotal + damagePointsFinal;
    if (location === "turret") {
      tank.damageTurret = tank.damageTurret + damagePointsFinal;
    } else if (location === "side") {
      tank.damageSide = tank.damageSide + damagePointsFinal;
    } else if (location === "weakspot") {
      tank.damageWeakspot = tank.damageWeakspot + damagePointsFinal;
    } else {
      tank.damageHull = tank.damageHull + damagePointsFinal;
    }
  }

  public awardKou(roster: Tank[], winnings: number, channel: TextChannel) {
    const walletHelper: WalletHelper = new WalletHelper();
    const gupdtWalletDao: GuPDTWalletDao = new GuPDTWalletDao();
    let amount = Math.round(winnings * 100 / roster.length);

    roster.forEach(async teamMember => {
      if (!teamMember.isAiCommander) {
        let wallet = await walletHelper.getWallet(teamMember.commander.id, channel.guild.id);
        wallet.balance = wallet.balance + amount;
        await gupdtWalletDao.updateFunds(wallet);
        channel.send((`${teamMember.commander} has been awarded \`甲${walletHelper.formatKou(amount)}\` for this battle`));
      }
    })
  }
}