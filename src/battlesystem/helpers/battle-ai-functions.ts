import { Tank } from "../model/tank";
import { Battle } from "../model/battle";
import { BattleMain } from "../battle-main";

export class BattleAIFunctions {
    startTurnAct(tank: Tank, battle: Battle, service: BattleMain) {
        // Find and fire at Target
        let enemyTeam = tank.targetId.includes("A") ? battle.teamB : battle.teamA;
        let chosenTarget: Tank = null;
        let target = ``;
        let location = ``;
        if (enemyTeam.length === 1) {
            target = enemyTeam[0].targetId;
            chosenTarget = enemyTeam[0];
        } else {
            let randomHit = Math.floor(Math.random() * (10 - 1 + 1) + 1);
            if (randomHit <= 5) {
                let randomTarget = Math.floor(Math.random() * ((enemyTeam.length - 1) - 0 + 1) + 0);
                target = enemyTeam[randomTarget].targetId;
                chosenTarget = enemyTeam[randomTarget];
            } else {
                let hitTarget = 100;
                let targetHealth = 1000;
                enemyTeam.forEach(enemyTank => {
                    if (enemyTank.hitThreshhold < hitTarget) {
                        hitTarget = enemyTank.hitThreshhold;
                        target = enemyTank.targetId;
                        targetHealth = enemyTank.health;
                        chosenTarget = enemyTank;
                    } else if (enemyTank.hitThreshhold === hitTarget) {
                        if (enemyTank.health < targetHealth) {
                            hitTarget = enemyTank.hitThreshhold;
                            target = enemyTank.targetId;
                            targetHealth = enemyTank.health;
                            chosenTarget = enemyTank;
                        }
                    }
                })
            }
        }

        if (chosenTarget.hitThreshhold + 3 < 9 && !tank.tracked && !tank.special.includes("SlowButSure")) {
            let randomAim = Math.floor(Math.random() * (10 - 1 + 1) + 1);
            if (randomAim <= 5) {
                if (!chosenTarget.tracked) {
                    location = ` tracks`;
                } else if (!chosenTarget.special.includes("PaperWalls")) {
                    location = ` weakspot`;
                } else {
                    location = chosenTarget.special.includes("PaperWalls") ? ` hull`:` side`;
                }
            }
        }

        let attackOrder = `!battle target ${target}${location}`

        setTimeout(() => {
            service.targetTank(battle.channel, null, attackOrder, tank)
        }, 5000);
    }
}