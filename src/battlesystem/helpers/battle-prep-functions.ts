import { Battle } from "../model/battle";
import { TextChannel, GuildMember, Role, Message } from "discord.js";
import { tankNames, tankStats } from "./../tanklist.json";
import { Tank } from "./../model/tank";
import { BattleMiscFunctions } from "./battle-misc-functions";
import { BattleMessages } from "../messages/battle-messages";
import { GuPDTUserDao } from "../../database/dao/gupdt-user-dao";
import { GuPDTUser } from "../../database/entity/gupdt-user";
import { schoolroles } from "../../database/school-list.json"
import { BattleMain } from "../battle-main";

export class BattlePreparationFunctions {
    helper: BattleMiscFunctions = new BattleMiscFunctions();
    battleMessage: BattleMessages = new BattleMessages();

    // PREPARE A NEW BATTLE
    public prepareBattle(channel: TextChannel, author: GuildMember, battles: Battle[], message: Message): boolean {
        // CHECK IF THERE IS A BATTLE IN THAT CHANNEL ALREADY
        if (this.helper.findBattle(channel, battles) != null) {
          return false;
        }
        let newBattle = new Battle();
        newBattle.owner = author;
        newBattle.channel = channel;
        newBattle.message = message;
        battles.push(newBattle);
    
        return true;
      }
    
      // JOIN BATTLE AS COMMANDER
      public async joinBattle(channel: TextChannel, author: GuildMember, content: string, battles: Battle[], message: Message, aiCommander: boolean): Promise<string> {
        let battle = this.helper.findBattle(channel, battles);
    
        if (battle == null) {
          return `(There is currently no Battle being prepared!)`;
        }
        if (battle.started) {
          return `(The current Battle has already started!)`;
        }
        if (!aiCommander && battle.participants.includes(author)) {
          return `(You have already joined this Battle!)`;
        }
        // clean String
        let cleanString = content.substring(13);
    
        // define Team
        let teamString = cleanString.substring(0, 1);
        if (teamString.toUpperCase() !== "A" && teamString.toUpperCase() !== "B") {
          return "(Please input A or B as team)";
        }
    
        let tankString = cleanString.substring(2);
        let tankList: string[] = [];
        tankList = tankList.concat(tankNames.nameListMedium);
        tankList = tankList.concat(tankNames.nameListLight);
        tankList = tankList.concat(tankNames.nameListHeavy);
        tankList = tankList.concat(tankNames.nameListTankDestroyer);
        tankList = tankList.concat(tankNames.nameListSuperHeavy);
        tankList = tankList.concat(tankNames.nameListMBT);

        let found = false;
        for(let i = 0; i < tankList.length; i++) {
          let regex = new RegExp (tankList[i], "i")
          if (regex.test(tankString)) {
            found = true;
            tankString = tankList[i];
            break;
          }
        }
        if (!found) {
          return `(Please choose a Tank from the Tanklist! \`?battle tanklist\`)`;
        }

        let user: GuPDTUser = null;
        if (!aiCommander) {
          let gupdtUserDao: GuPDTUserDao  = new GuPDTUserDao()
          user = await gupdtUserDao.getUser(message.member.id, message.guild.id)
  
          if (user === undefined) {
            user = new GuPDTUser();
            let school = "None"
              author.roles.cache.forEach(role => {
                  if (schoolroles.includes(role.name)) {
                      school = role.name;
                  }
              })
            let gupdtUser = new GuPDTUser();
            gupdtUser.user = message.member.id;
            gupdtUser.guild = message.guild.id;
            gupdtUser.school = school;
            gupdtUser.battlesWon = 0;
            gupdtUser.battlesLost = 0;
            gupdtUser.blessings = 0;
            gupdtUser.crewXp = 0;
            user = await gupdtUserDao.addUser(gupdtUser);
          }
        }

        battle.participants.push(author);
        
        let newTank = new Tank();
        newTank.commander = author;
        newTank.commanderName = aiCommander ? "" : author.displayName;
        newTank.isAiCommander = aiCommander;
        newTank.tankName = tankString;
        newTank.health = tankStats[tankString].health;
        newTank.hitThreshhold = tankStats[tankString].hitThreshold;
        newTank.special = tankStats[tankString].special;
        newTank.cooldown = 0;
        newTank.type = tankStats[tankString].type;
        newTank.crewxp = user != null ? user.crewXp : 0;
        newTank.misses = 1;
        newTank.damageHull = 0;
        newTank.damageSide = 0;
        newTank.damageTotal = 0;
        newTank.damageTurret = 0;
        newTank.damageWeakspot = 0;
    
        if (teamString === "A" || teamString === "a") {
            battle.teamA.push(newTank)
        } else {
            battle.teamB.push(newTank)
        }
    
        if (aiCommander) {
          return (`(${author.displayName} added AI to Team ${teamString} with a ${tankString} (HP: ${newTank.health} | HT: ${newTank.hitThreshhold})`);
        }
        return (`(${author.displayName} joined Team ${teamString} with a ${tankString} (HP: ${newTank.health} | HT: ${newTank.hitThreshhold})`);
      }
    
      // LEAVE BATTLE PREPERATION
      public leaveBattle(channel: TextChannel, author: GuildMember, battles: Battle[]): string {
        let battle = this.helper.findBattle(channel, battles);
    
        if (battle == null) {
          return `(There is currently no Battle for you to leave)`;
        }
        if (battle.started) {
          return `(The Battle has already started, use \`?battle surrender\` to leave early)`;
        }
        if (!battle.participants.includes(author)) {
          return `(You have not joined the Battle preparations)`;
        }
    
        this.helper.removeFromBattle(battle, author, null);
    
        return `(${author.displayName} has left the Battle preparations)`;
      }
    
      // CANCEL BATTLE
      public cancelBattle(channel: TextChannel, author: GuildMember, battles: Battle[]): string {
    
        let battle = this.helper.findBattle(channel, battles)
    
        if (battle == null) {
          return `(There is no battle to cancel)`;
        }
    
        let isAdmin = false;
        author.roles.cache.forEach((value: Role) => {
          if (value.name === "Admin") {
            isAdmin = true;
          }
        })
    
        if (battle.owner.id !== author.id && !isAdmin) {
          return `(You do not have permission to cancel the current Battle)`;
        }
    
        let index = battles.indexOf(battle);
        battles.splice(index, 1);
    
        return "(**-- BATTLE CANCELLED --**)"
      }
    
      // START BATTLE
      public startBattle(channel: TextChannel, author: GuildMember, battles: Battle[], service: BattleMain): boolean {
        let battle = this.helper.findBattle(channel, battles);
    
        let isAdmin = false;
        author.roles.cache.forEach((value: Role) => {
          if (value.name === "Admin" || value.name === "Technician") {
            isAdmin = true;
          }
        })

        if (!isAdmin) {
          if (!battle.participants.includes(author)) {
            return false;
          }
        }
    
        battle.started = true;

        let i = 0;
        battle.teamA.forEach(tank => {
          tank.targetId = "A" + i;
            if (tank.isAiCommander) {
                tank.commanderName = tank.tankName + "(" + tank.targetId + ")[AI]"
            }
          i++;
        });
        i = 0;
        battle.teamB.forEach(tank => {
          tank.targetId = "B" + i;
          if (tank.isAiCommander) {
              tank.commanderName = tank.tankName + "(" + tank.targetId + ")[AI]"
          }
          i++;
        })

        battle.rosterTeamA = [];
        battle.rosterTeamB = [];
        battle.rosterTeamA = battle.rosterTeamA.concat(battle.teamA);
        battle.rosterTeamB = battle.rosterTeamB.concat(battle.teamB);
        setTimeout(() => {
          let allTanks: Tank[] = [];

          allTanks = allTanks.concat(battle.teamA);
          allTanks = allTanks.concat(battle.teamB);

          allTanks.forEach(async tank => {
            if (tank.isAiCommander) {
              service.rollInitiative(channel, author, true, tank)
              await new Promise(r => setTimeout(r, 1000));
            }
          })
        }, 1000);
        
        this.battleMessage.displayTeams(battle);
        battle.channel.send(this.battleMessage.initiativePhase())
        return true;
      }
}