import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleConfirmCommand = new Command()
  .match(matchPrefixes("confirm"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);
    service.confirm(message.channel as TextChannel, message.member);
  });
