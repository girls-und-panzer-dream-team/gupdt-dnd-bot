import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleSurrenderCommand = new Command()
  .match(matchPrefixes("surrender"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    service.surrender(message.channel as TextChannel, message.member);
  });
