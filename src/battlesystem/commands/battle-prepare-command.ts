import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";
import { BattleMain6D } from "../../battlesystem-6d/battle-main";

export const BattlePrepareCommand = new Command()
  .match(matchPrefixes("prepare"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);
    const service6d = manager.getService(BattleMain6D); 

    if (service.channels.includes(message.channel.id)){
      if (!service6d.noBattleActive(message.channel as TextChannel)) {
        return message.channel.send("(**THERE IS A CLASSIC BATTLE BEING FOUGHT IN THIS CHANNEL ALREADY**)")
      }
      let battleValid = service.prepareBattle(message.channel as TextChannel, message.member, message);

      if (battleValid) {
        return message.channel.send(
          "*Welcome to GuPDT's Advanced RP Battle System*\n" +
          "**-- PREPARING BATTLE --**\n" + 
          "Participants join with `?battle join [team] [tank]` (Team: A, B)\n" +
          "To see a list of available Tanks use `?battle tanklist`\n" +
          "To see a list of commands use `?battle help`"
        );
      }
      return message.channel.send(
        "(**THERE IS AN UNRESOLVED BATTLE GOING ON IN THIS CHANNEL**)"
      );
    }
  });
