import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
export const BattleAdvancedCommand = new Command()
  .match(matchPrefixes("advanced"))
  .use((context) => {
    const { manager, message } = context;
    message.channel.send("**ADVANCED BATTLE FEATURES**\n"+
    "- Tracking of Battlestats (Wins, Losses, Crew XP)\n"+
    "- Crew XP can influence near-miss and barely-hit results as well as Flanking shots and Angling your Tank\n"+
    "- Adjustment of Aiming rolls to avoid 'endless Misses'\n"+
    "- AI Controlled opponents - Admins can spawn these in for you\n"+
    "- Domination / School matches that influence your monthly allowance\n" +
    "- Tracking of Battledamage\n\n" +
    "- Additional Tactical Commands such as 'Hull-Down'\n")
    return message.channel.send(
    "**PLANNED FEATURES**\n"+
    "- Training battles that allow you to get a small amout of crew XP / Test your tank\n"+
    "- Modifiers according to the map that is being fought on\n"+
    "- Permanent School Tanklists and repair system\n"+
    "- Personal Tank customization\n"
    );
  });
