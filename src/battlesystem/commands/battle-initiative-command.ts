import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleInitiativeCommand = new Command()
  .match(matchPrefixes("initiative"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);
    service.rollInitiative(message.channel as TextChannel, message.member, false, null);
  });
