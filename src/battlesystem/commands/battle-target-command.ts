import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleTargetCommand = new Command()
  .match(matchPrefixes("target"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    service.targetTank(message.channel as TextChannel, message.member, message.content, null);
  });
