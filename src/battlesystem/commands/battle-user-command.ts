import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { GuPDTUserDao } from "../../database/dao/gupdt-user-dao";
import { GuPDTUser } from "../../database/entity/gupdt-user";
import { schools, schoolroles } from "../../database/school-list.json"

export const BattleUserCommand = new Command()
  .match(matchPrefixes("user"))
  .use(async (context) => {
    const { manager, message } = context;

    const guPDTUserDao = new GuPDTUserDao();
    let user = await guPDTUserDao.getUser(message.member.id, message.guild.id);

    if (user != null){
        let guildMember = await message.guild.members.fetch(user.user)
        message.channel.send(`\`\`\`User:           ${guildMember.displayName}\nServer:         ${message.guild.name}\nSchool:         ${schools[user.school].name}\nBattles Won:    ${user.battlesWon}\nBattles Lost:   ${user.battlesLost}\nCrew XP:        ${user.crewXp}\`\`\``)
    } else {
        message.channel.send(`(${message.member.displayName} is not stored in the Database! Adding now...)`)
            
        let school = "None"
        message.member.roles.cache.forEach(role => {
            if (schoolroles.includes(role.name)) {
                school = role.name;
            }
        })
        let gupdtUser = new GuPDTUser();
        gupdtUser.user = message.member.id;
        gupdtUser.guild = message.guild.id;
        gupdtUser.school = school;
        gupdtUser.battlesWon = 0;
        gupdtUser.battlesLost = 0;
        gupdtUser.blessings = 0;
        gupdtUser.crewXp = 0;
        let success = await guPDTUserDao.addUser(gupdtUser);
        message.channel.send(`(${success ? "User added" : "Error adding user!"})`)
    }
});
