import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleCancelCommand = new Command()
  .match(matchPrefixes("cancel"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    let cancelValid = service.cancelBattle(message.channel as TextChannel, message.member);

    return message.channel.send(cancelValid);
  });
