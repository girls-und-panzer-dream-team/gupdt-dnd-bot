import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleSkipCommand = new Command()
  .match(matchPrefixes("skip"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    service.skip(message.channel as TextChannel, message.member);
  });
