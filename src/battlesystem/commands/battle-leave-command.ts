import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleLeaveCommand = new Command()
  .match(matchPrefixes("leave"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    if (service.channels.includes(message.channel.id)){
      let leaveSuccess = service.leaveBattle(message.channel as TextChannel, message.member);

      return message.channel.send(leaveSuccess);
    }
  });
