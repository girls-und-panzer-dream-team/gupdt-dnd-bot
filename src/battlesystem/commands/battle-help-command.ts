import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { BattleMain } from "../battle-main";

export const BattleHelpCommand = new Command()
  .match(matchPrefixes("help"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    if (service.channels.includes(message.channel.id)){
      message.channel.send("This System is designed to allow Players to have Tank Battles with advanced rules and rewards.\n It uses the original Ruleset with additional features\n use `?battle advanced` to learn about the differences\n"+
      "use `!battle help` to learn about the Classic Battle System\n" +
      "After Battle preparation the intented use is:\n\n" +
      " - RP out the previous result AGAINST you - and your next move\n" +
      " - execute your action with the `?battle`- commands\n" +
      " - Look at the results the Bot presents you\n" +
      " - intigrate the Result into your Roleplay\n" + 
      " - await your Partner(s) turns\n - AND REPEAT\n\n")

      return message.channel.send("**-- LIST OF COMMANDS FOR GUPDT RP BATTLE SYSTEM --**\n" +
      "__BATTLE PREPARATION:__\n"+
      "`?battle prepare` - Hosts a new Battle in the Channel\n" +
      "`?battle cancel` - Cancels the ongoing/prepared Battle - __Only the Match Creator or an Admin can Cancel a Battle__\n" +
      "`?battle join [team] [tank]` *(Team: A, B)* - Adds you to a Battle that is in Preparation Phase\n" +
      "`?battle tanklist` - Shows a list of available Tanks and the name to be used for `!battle join`\n" +
      "`?battle leave` - Removes you from a Battle that is in Preparation Phase\n" +
      "`?battle start` - Starts the Battle - __Both Teams must have at least 1 Commander__ - once a Battle has started it is not possible to join anymore\n" +
      "__BATTLE PROCESS:__\n" +
      "`?battle initiative` - The Battle starts in the Initiative phase, all participants must roll for initiative to determine turn-order\n" +
      "`?battle target [target-id] [location]` *(location: hull, turret, tracks, side, weakspot (location is optional: 'See Aiming Rule')* - used to fire at targets\n" +
      "`?battle position hulldown` - use your turn to attempt to Hulldown - forcing other Tanks to aim for your turret - or to avoid being flanked\n" +
      "`?battle position flank` - use your turn to attempt to flank the enemy - circumventing hulldown tanks or making aimed shots easier\n" +
      "`?battle skip` - used to skip the rest of your turn\n" +
      "`?battle confirm` - used to confirm use of special abilities not on your turn\n" +
      "`?battle deny` - used to deny use of special abilities not on your turn\n" +
      "`?battle special` - used to activate specials that are activated manually on your turn\n" +
      "`?battle status` - Displays the status of remaining Tanks and their Target-ID in the Battle\n" +
      "`?battle surrender` - Manually releases the White Flag of your Tank and removes you from Battle\n" +
      "__MISCELLANEOUS:__\n" +
      "`?battle adai [team] [tank]`- Allows Admins to add AI controlled Tanks\n" +
      "`?battle user` - Displays battle statistics about yourself\n" +
      "`?battle credits` - If you'd like to know who is responsible for this System\n" +
      "`?battle help` - Shows all of the above!\n"
      );
    }
  });
