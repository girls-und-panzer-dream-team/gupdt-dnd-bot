import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattlePositionCommand = new Command()
  .match(matchPrefixes("position"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    service.position(message.channel as TextChannel, message.member, message.content);
  });
