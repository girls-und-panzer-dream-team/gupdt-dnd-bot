import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleStartCommand = new Command()
  .match(matchPrefixes("start"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    if (service.channels.includes(message.channel.id)){
      if (!service.canStartBattle(message.channel as TextChannel)) {
        return message.channel.send(
          "(Not enough Players to start the Battle, at least one tank per Team is required)"
        );
      }

      service.startBattle(message.channel as TextChannel, message.member);
    }
  });
