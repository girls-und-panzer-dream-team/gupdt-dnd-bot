import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleStatusCommand = new Command()
  .match(matchPrefixes("status"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);

    service.battleStatus( message.channel as TextChannel);
  });
