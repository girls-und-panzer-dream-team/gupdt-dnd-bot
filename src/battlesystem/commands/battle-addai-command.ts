import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel, Role } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleAddAICommand = new Command()
  .match(matchPrefixes("adai"))
  .use(async (context) => {
    const { manager, message } = context;
    let isAdmin = false;
    message.member.roles.cache.forEach((value: Role) => {
      if (value.name === "Admin" || value.name === "Technician") {
        isAdmin = true;
      }
    })
    if (isAdmin) {
      const service = manager.getService(BattleMain);

      if (service.channels.includes(message.channel.id)){
        let joinSuccess = await service.addAiToBattle(message.channel as TextChannel, message.member, message.content, message);

        if (service.canStartBattle(message.channel as TextChannel)) {
          return message.channel.send(joinSuccess + "\n(Both Teams have at least one Tank use `?battle start` to begin!)");
        }
        return message.channel.send(joinSuccess);
      }
    }
  });
