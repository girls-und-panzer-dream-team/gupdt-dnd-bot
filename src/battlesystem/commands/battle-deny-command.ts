import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../battle-main";

export const BattleDenyCommand = new Command()
  .match(matchPrefixes("deny"))
  .use((context) => {
    const { manager, message } = context;
    const service = manager.getService(BattleMain);
    service.deny(message.channel as TextChannel, message.member);
  });
