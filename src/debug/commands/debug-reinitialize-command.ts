import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { BattleMain } from "../../battlesystem/battle-main";

export const DebugReinitializeCommand = new Command()
  .match(matchPrefixes("reinit"))
  .use((context) => {
    const { manager, message } = context;
    manager.getService(BattleMain).channels = [];
    let battleChannels: string[] = [];
    let managedChannels: string[] = [];

    message.client.guilds.cache.forEach(guild => {
        guild.channels.cache.forEach(channel => {
            if (channel.parent != null && channel.type === "text") {
                let textChannel = channel as TextChannel
                if (textChannel.topic != null && textChannel.topic.includes("[rp-b]")) {
                    manager.getService(BattleMain).channels.push(channel.id);
                    if (textChannel.guild.id === message.guild.id) {
                        battleChannels.push(channel.name);
                    }
                }
                if (textChannel.topic != null && textChannel.topic.includes("[rp-m]")) {
                    if (textChannel.guild.id === message.guild.id) {
                        managedChannels.push(channel.name);
                    }
                }
            }
        })
    })
    message.channel.send(`***Bot reinitialized***\nBattle Channels: \`${battleChannels.toString()}\`\nManaged Channels: \`${managedChannels.toString()}\``);
});
