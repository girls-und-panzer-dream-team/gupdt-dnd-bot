import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";

export const DebugServersCommand = new Command()
  .match(matchPrefixes("servers"))
  .use((context) => {
    const { manager, message } = context;

    if (message.guild.id === "739413387846483979") {
        let displayString = ``;
        message.client.guilds.cache.forEach(guild => {
            displayString = `${displayString}ID: ${guild.id} - ${guild.name}\n`
            guild.channels.cache.forEach(channel => {
                if (channel.parent != null && channel.type === "text") {
                    let textChannel = channel as TextChannel
                    if (textChannel.topic != null && textChannel.topic.includes("[rp-b]")) {
                        displayString = `${displayString}- ID: ${channel.id} - ${channel.name} - BattleChannel\n`
                    }
                    if (textChannel.topic != null && textChannel.topic.includes("[rp-m]")) {
                        displayString = `${displayString}- ID: ${channel.id} - ${channel.name} - ManagedChannel\n`
                    }
                    if (displayString.length >= 1900) {
                        message.channel.send(displayString);
                        displayString = ``;
                    }
                }
            })
            message.channel.send(displayString);
            displayString = ``;
        })
    }
  });
