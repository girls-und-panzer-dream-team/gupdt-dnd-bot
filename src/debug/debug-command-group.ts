import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { DebugServersCommand } from "./commands/debug-servers-command"
import { DebugReinitializeCommand } from "./commands/debug-reinitialize-command"

export const DebugCommandGroup = new CommandGroup()
  .match(matchPrefixes("!debug"))
  .setCommands(DebugServersCommand, DebugReinitializeCommand)