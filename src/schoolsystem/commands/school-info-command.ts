import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { GuPDTSchoolDao } from "../../database/dao/gupdt-school-dao";
import { schools, schoolroles } from "../../database/school-list.json"
import { GuPDTSchool } from "../../database/entity/gupdt-school";

export const SchoolInfoCommand = new Command()
  .match(matchPrefixes("info"))
  .use(async (context) => {
    const { manager, message } = context;
    
    const gupdtSchoolDao: GuPDTSchoolDao = new GuPDTSchoolDao();
    let schoolRole = "None"
    message.member.roles.cache.forEach(role => {
      if (schoolroles.includes(role.name)) {
        schoolRole = role.name;
      }
    })

    let school = await gupdtSchoolDao.getSchoolBySchoolRole(schoolRole, message.guild.id);
    
    if (school === undefined) {
      let newSchool = new GuPDTSchool();
      newSchool.guild = message.guild.id;
      newSchool.schoolName = schools[schoolRole].name;
      newSchool.schoolRole = schoolRole;
      newSchool.schoolScore = 0;
      console.log(newSchool);
      school = await gupdtSchoolDao.addSchool(newSchool);
    }

    message.channel.send(`\`\`\`SCHOOL INFORMATION FOR ${school.schoolName.toUpperCase()}
Sensha-Dó Score: ${school.schoolScore}
Tanks: - COMING SOON -
\`\`\`
  `)
  });
