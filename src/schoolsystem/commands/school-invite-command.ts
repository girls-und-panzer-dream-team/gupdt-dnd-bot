import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { VisitorHelper } from "../helpers/visitor-helper";
import { GuPDTInviteDao } from "../../database/dao/gupdt-invite-dao";
import { GuPDTInvite } from "../../database/entity/gupdt-invite";

export const SchoolInviteCommand = new Command()
  .match(matchPrefixes("invite"))
  .use(async (context) => {
    const { manager, message } = context;
    const visitorHelper: VisitorHelper = new VisitorHelper();
    const gupdtInviteDao: GuPDTInviteDao = new GuPDTInviteDao();

    let targetChannel = message.channel as TextChannel
    if (targetChannel.topic.includes("[rp-port]")) {
        if (message.mentions.members.size !== 1 || (message.mentions.members.size === 1 && message.mentions.members.first().id === message.member.id)) {
            return message.channel.send(`(Invalid recipients, ${message.member.displayName} - choose one @User!)`);
        }
        const inviterole = visitorHelper.findVisitorRole(message.member);
        if (inviterole === "NONE") {
            return message.channel.send(`(Could not find valid visitor role for your school, ${message.member.displayName}!)`);
        }
        const inviteeinviterole = visitorHelper.findVisitorRole(message.mentions.members.first());
        if (inviterole === inviteeinviterole) {
            return message.channel.send(`(You belong to the same school, ${message.member.displayName}!)`);
        }

        let invites = await gupdtInviteDao.getAllInvites();
        let inviteFound = false;
        for (let i = 0; i < invites.length; i++) {
            if (invites[i].guild === message.guild.id && invites[i].user === message.mentions.members.first().id && invites[i].inviteRole === inviterole) {
                inviteFound = true;
            }
        }
        if (inviteFound) {
            return message.channel.send(`(${message.mentions.members.first().displayName} is already invited to your school ${message.member.displayName}!)`);
        }
        let invite: GuPDTInvite = new GuPDTInvite();

        invite.guild = message.guild.id;
        invite.inviteDate = new Date();
        invite.inviteDate.setDate(invite.inviteDate.getDate() + 2);
        invite.inviteRole = inviterole;
        invite.user = message.mentions.members.first().id;

        let returnValue = await gupdtInviteDao.addInvite(invite);

        if (returnValue != undefined) {
            let actualRole = message.guild.roles.cache.find(role => role.name === inviterole);
            message.mentions.members.first().roles.add(actualRole);
            return message.channel.send(`(${message.mentions.members.first().displayName} was invited to your school ${message.member.displayName}!)`);
        }
    }
  });
