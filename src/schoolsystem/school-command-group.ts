import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { SchoolInfoCommand } from "./commands/school-info-command"
import { SchoolInviteCommand } from "./commands/school-invite-command"
export const SchoolCommandGroup = new CommandGroup()
  .match(matchPrefixes("!school"))
  .setCommands(SchoolInfoCommand, SchoolInviteCommand)