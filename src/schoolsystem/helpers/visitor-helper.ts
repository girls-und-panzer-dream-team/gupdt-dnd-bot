import { GuildMember } from "discord.js";
import { schoolroles, schools } from "../../database/school-list.json"
import { GuPDTInviteDao } from "../../database/dao/gupdt-invite-dao";
import { Bot } from "@enitoni/gears-discordjs";

export class VisitorHelper {
    gupdtInviteDao: GuPDTInviteDao = new GuPDTInviteDao()

    public findVisitorRole(member: GuildMember): string {
        let foundRole = "NONE"
        member.roles.cache.forEach(role => {
            if (schoolroles.includes(role.name)) {
                foundRole = schools[role.name].visitorRole;
            }
        })
        return foundRole;
    }

    public async clearInvites(bot: Bot) {
        let invites = await this.gupdtInviteDao.getAllInvites();
        let date = Date.now();
        invites.forEach(async invite => {
            if (invite.inviteDate.valueOf() < date) {
                const guildRef = bot.client.guilds.cache.find(guild => guild.id === invite.guild);
                const userRef = guildRef.members.cache.find(user => user.id === invite.user);
                const roleRef = guildRef.roles.cache.find(role => role.name === invite.inviteRole);
                userRef.roles.remove(roleRef);
                await this.gupdtInviteDao.removeInvite(invite);
            }
        });
    }
}