import { GuPDTWallet } from "../../database/entity/gupdt-wallet";
import { GuPDTWalletDao } from "../../database/dao/gupdt-wallet-dao";
export class WalletHelper {
    public async getWallet(memberid: string, guildid: string): Promise<GuPDTWallet> {
        const gupdtWalletDao: GuPDTWalletDao = new GuPDTWalletDao();
        let wallet: GuPDTWallet = await gupdtWalletDao.getWallet(memberid, guildid);
        if (wallet === undefined) {
            const date = new Date()
            let newWallet: GuPDTWallet = new GuPDTWallet();
            newWallet.user = memberid;
            newWallet.guild = guildid;
            newWallet.balance = 200;
            newWallet.lastAllowanceMonth = date.getMonth();
            newWallet.lastAllowanceYear = date.getFullYear();
            return await gupdtWalletDao.updateFunds(newWallet);
        }
        return wallet;
    }

    public formatKou(value: number): string {
        return Number(Math.round(parseFloat(value + 'e' + 2)) + 'e-' + 2).toFixed(2)
    }
}