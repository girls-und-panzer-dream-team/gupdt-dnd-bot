import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { WalletHelper } from "../helpers/wallet-helper";
import { GuPDTWalletDao } from "../../database/dao/gupdt-wallet-dao";
import { GuPDTWallet } from "../../database/entity/gupdt-wallet";

export const WalletBalanceCommand = new Command()
  .match(matchPrefixes("balance"))
  .use(async (context) => {
    const { manager, message } = context;
    const walletHelper: WalletHelper = new WalletHelper();
    const gupdtWalletDao: GuPDTWalletDao = new GuPDTWalletDao();
    let wallet: GuPDTWallet = await walletHelper.getWallet(message.member.id, message.guild.id);

    return message.channel.send(`(You have \`甲${walletHelper.formatKou(wallet.balance)}\` in your account, ${message.member.displayName}.)`);
  });
