import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { WalletHelper } from "../helpers/wallet-helper";
import { GuPDTWalletDao } from "../../database/dao/gupdt-wallet-dao";
import { GuPDTWallet } from "../../database/entity/gupdt-wallet";
import { Role } from "discord.js";

export const WalletAwardCommand = new Command()
  .match(matchPrefixes("award"))
  .use(async (context) => {
    const { manager, message } = context;
    let isAdmin = false;
    message.member.roles.cache.forEach((value: Role) => {
      if (value.name === "Admin" || value.name === "Technician") {
        isAdmin = true;
      }
    })
    if (isAdmin) {
        let cleanString = message.content.substring(12).replace(/\s+/g, " ").trim();
        let args = cleanString.split(" ");
        let value = Number.parseFloat(args[1])

        if (message.mentions.members.size !== 1 || (message.mentions.members.size === 1 && message.mentions.members.first().id === message.member.id)) {
            return message.channel.send(`(Invalid recipients, ${message.member.displayName} - choose one @User!)`)
        }
        if (isNaN(value) || value < 0) {
            return message.channel.send(`(\`${args[1]}\` is not a valid number, ${message.member.displayName}!)`)
        }

        const walletHelper: WalletHelper = new WalletHelper();
        const gupdtWalletDao: GuPDTWalletDao = new GuPDTWalletDao();

        let recievingWallet: GuPDTWallet = await walletHelper.getWallet(message.mentions.members.first().id, message.guild.id);

        recievingWallet.balance = recievingWallet.balance + value;

        await gupdtWalletDao.updateFunds(recievingWallet);
        
        return message.channel.send(`(${message.member.displayName} awarded \`甲${walletHelper.formatKou(value)}\` to ${message.mentions.members.first().displayName})`);
    }
  });
