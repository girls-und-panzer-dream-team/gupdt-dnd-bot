import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { WalletHelper } from "../helpers/wallet-helper";
import { GuPDTWalletDao } from "../../database/dao/gupdt-wallet-dao";
import { GuPDTWallet } from "../../database/entity/gupdt-wallet";

export const WalletPayCommand = new Command()
  .match(matchPrefixes("pay"))
  .use(async (context) => {
    const { manager, message } = context;

    let cleanString = message.content.substring(12).replace(/\s+/g, " ").trim();
    let args = cleanString.split(" ");
    let value = Number.parseFloat(args[1])

    if (message.mentions.members.size !== 1 || (message.mentions.members.size === 1 && message.mentions.members.first().id === message.member.id)) {
      return message.channel.send(`(Invalid recipients, ${message.member.displayName} - choose one @User!)`)
    }
    if (isNaN(value) || value < 0) {
      return message.channel.send(`(\`${args[1]}\` is not a valid number, ${message.member.displayName}!)`)
    }

    const walletHelper: WalletHelper = new WalletHelper();
    const gupdtWalletDao: GuPDTWalletDao = new GuPDTWalletDao();

    let wallet: GuPDTWallet = await walletHelper.getWallet(message.member.id, message.guild.id);

    if (wallet.balance < value) {
      return message.channel.send(`(You do not have enough money to give \`甲${walletHelper.formatKou(value)}\`, ${message.member.displayName}!)`)
    }
    let recievingWallet: GuPDTWallet = await walletHelper.getWallet(message.mentions.members.first().id, message.guild.id);
    recievingWallet.balance = recievingWallet.balance + value;
    wallet.balance = wallet.balance - value;
    await gupdtWalletDao.updateFunds(wallet);
    await gupdtWalletDao.updateFunds(recievingWallet);
    return message.channel.send(`(${message.member.displayName} gave \`甲${walletHelper.formatKou(value)}\` to ${message.mentions.members.first().displayName})`);
  });
