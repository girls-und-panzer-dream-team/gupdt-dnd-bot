import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";

export const WalletHelpCommand = new Command()
  .match(matchPrefixes("help"))
  .use((context) => {
    const { manager, message } = context;
    return message.channel.send(`
    **-- LIST OF COMMANDS FOR GUPDT WALLET SYSTEM --**
    **Currency format for [amount]**: \`xx.xx\`
    \`!wallet allowance\` - You may once a month request the allowance from your school your allowance is based of a base value and your schools sensha-do score
    \`!wallet balance\` - Show you how much money you currently have
    \`!wallet pay @[target] [amount]\` - pay the person you ping amount of money
    \`!wallet donate [amount]\` - some community Events can be donated to in their respective channels
    \`!wallet help\` - Shows the above output!
    `);
  });
