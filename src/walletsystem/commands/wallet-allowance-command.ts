import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { GuPDTWallet } from "../../database/entity/gupdt-wallet";
import { WalletHelper } from "../helpers/wallet-helper";
import { GuPDTWalletDao } from "../../database/dao/gupdt-wallet-dao";
import { GuPDTSchoolDao } from "../../database/dao/gupdt-school-dao";
import { schools, schoolroles } from "../../database/school-list.json"
import { GuPDTSchool } from "../../database/entity/gupdt-school";

export const WalletAllowanceCommand = new Command()
  .match(matchPrefixes("allowance"))
  .use(async (context) => {
    const { manager, message } = context;
    const walletHelper: WalletHelper = new WalletHelper();
    const gupdtWalletDao: GuPDTWalletDao = new GuPDTWalletDao();
    let wallet: GuPDTWallet = await walletHelper.getWallet(message.member.id, message.guild.id);
    const date = new Date();
    if (wallet.lastAllowanceYear === date.getFullYear() && wallet.lastAllowanceMonth === date.getMonth()) {
      return message.channel.send(`(You've already recieved your school-allowance for this month, ${message.member.displayName}!)`);
    } else {
      const gupdtSchoolDao: GuPDTSchoolDao = new GuPDTSchoolDao();
      let schoolRole = "None"
      message.member.roles.cache.forEach(role => {
        if (schoolroles.includes(role.name)) {
          schoolRole = role.name;
        }
      })

      let school = await gupdtSchoolDao.getSchoolBySchoolRole(schoolRole, message.guild.id);
      
      if (school === undefined) {
        let newSchool = new GuPDTSchool();
        newSchool.guild = message.guild.id;
        newSchool.schoolName = schools[schoolRole].name;
        newSchool.schoolRole = schoolRole;
        newSchool.schoolScore = 0;
        console.log(newSchool);
        school = await gupdtSchoolDao.addSchool(newSchool);
      }

      let amount = 200 + school.schoolScore;

      wallet.balance = wallet.balance + amount;
      wallet.lastAllowanceMonth = date.getMonth();
      wallet.lastAllowanceYear = date.getFullYear();
      await gupdtWalletDao.updateFunds(wallet);
      return message.channel.send(`(Your School has added \`甲${walletHelper.formatKou(amount)}\` (\`200\` + School-Score: \`${school.schoolScore}\`) to your account, ${message.member.displayName}!)`);
    } 
  });
