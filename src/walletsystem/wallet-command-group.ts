import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { WalletHelpCommand } from "./commands/wallet-help-command"
import { WalletBalanceCommand } from "./commands/wallet-balance-command"
import { WalletDonateCommand } from "./commands/wallet-donate-command"
import { WalletAllowanceCommand } from "./commands/wallet-allowance-command"
import { WalletPayCommand } from "./commands/wallet-pay-command"
import { WalletAwardCommand } from "./commands/wallet-award-command"

export const WalletCommandGroup = new CommandGroup()
  .match(matchPrefixes("!wallet"))
  .setCommands(WalletHelpCommand, WalletBalanceCommand, WalletDonateCommand, WalletAllowanceCommand, WalletPayCommand, WalletAwardCommand)