import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";

export const EventHelpCommand = new Command()
  .match(matchPrefixes("help"))
  .use(async (context) => {
    const { manager, message } = context;

    return message.channel.send("");
  });
