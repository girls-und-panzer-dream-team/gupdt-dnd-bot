import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";

export const EventAddStageCommand = new Command()
  .match(matchPrefixes("addStage"))
  .use(async (context) => {
    const { manager, message } = context;

    return message.channel.send("");
  });
