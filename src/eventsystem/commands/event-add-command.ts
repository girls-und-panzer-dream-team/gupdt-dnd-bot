import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { GuPDTEventDao } from "../../database/dao/gupdt-event-dao";
import { EventHelper } from "../helpers/event-helper";
import { GuPDTEvent } from "../../database/entity/gupdt-event";

export const EventAddCommand = new Command()
  .match(matchPrefixes("add"))
  .use(async (context) => {
    const { manager, message } = context;
    const gupdtEventDao: GuPDTEventDao = new GuPDTEventDao();
    const eventHelper: EventHelper = new EventHelper();

    let event = await gupdtEventDao.getEvent(message.channel.id, message.guild.id);

    if (event !== undefined && event.active) {
      return;
    }

    let newEvent: GuPDTEvent = new GuPDTEvent();
    

    return;
  });
