import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { GuPDTEventDao } from "../../database/dao/gupdt-event-dao";
import { GuPDTEvent } from "../../database/entity/gupdt-event";
import { EventHelper } from "../helpers/event-helper";

export const EventStatusCommand = new Command()
  .match(matchPrefixes("status"))
  .use(async (context) => {
    const { manager, message } = context;
    const gupdtEventDao: GuPDTEventDao = new GuPDTEventDao();
    const eventHelper: EventHelper = new EventHelper();

    let event = await gupdtEventDao.getEvent(message.channel.id, message.guild.id);

    if (event === undefined) {
      return;
    } else if (!event.active) {
      return
    }
    let currentStage = eventHelper.getCurrentStage(event);
    if (currentStage === null) {
      return;
    }
    let command = currentStage.stageType === "donate" ? "!wallet donate [amount]" : "!event work";

    return message.channel.send(`\`\`\`
Status for event: ${event.eventName}
Stage:            ${currentStage.stageName}
Description:      ${currentStage.stageDescription}
Progress:         ${currentStage.stageProgress} / ${currentStage.stageTarget}
Type:             ${currentStage.stageType} - use \`${command}\` to help advance the stage!
\`\`\``);
  });
