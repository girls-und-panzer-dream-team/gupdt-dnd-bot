import { GuPDTEvent } from "../../database/entity/gupdt-event";
import { GuPDTEventStage } from "../../database/entity/gupdt-eventstage";

export class EventHelper {
  getCurrentStage(event: GuPDTEvent): GuPDTEventStage {
      for (let i = 0; i < event.stages.length; i++) {
          if (event.stages[i].stageActive) {
              return event.stages[i];
          }
      }
      return null;
  }

}