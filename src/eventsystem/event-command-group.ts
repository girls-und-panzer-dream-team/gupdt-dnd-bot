import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { EventAddCommand } from "./commands/event-add-command"
import { EventAddStageCommand } from "./commands/event-add-stage-command"
import { EventHelpCommand } from "./commands/event-help-command"
import { EventStatusCommand } from "./commands/event-status-command"
import { EventWorkCommand } from "./commands/event-work-command"

export const WalletCommandGroup = new CommandGroup()
  .match(matchPrefixes("!event"))
  .setCommands(EventAddCommand, EventAddStageCommand, EventHelpCommand, EventStatusCommand, EventWorkCommand)