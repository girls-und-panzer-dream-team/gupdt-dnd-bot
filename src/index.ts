import { Adapter, Bot } from "@enitoni/gears-discordjs"
import { token } from "../config.json"
import { BattleMain } from "./battlesystem/battle-main"
import { RacingMain } from "./racingsystem/racing-main"
import { BattleCommandGroup } from "./battlesystem/battle-command-group"
import { DebugCommandGroup } from "./debug/debug-command-group"
import { WalletCommandGroup } from "./walletsystem/wallet-command-group"
import { RacingCommandGroup } from "./racingsystem/racing-command-group"
import { TextChannel } from "discord.js"
import { Connection, createConnection } from "typeorm"
import ormConfig from "../ormconfig.json"
import { GuPDTUser } from "./database/entity/gupdt-user"
import { GuPDTTank } from "./database/entity/gupdt-tank"
import { GuPDTWallet } from "./database/entity/gupdt-wallet"
import { GuPDTEvent } from "./database/entity/gupdt-event"
import { GuPDTEventStage } from "./database/entity/gupdt-eventstage"
import { GuPDTEventUser } from "./database/entity/gupdt-eventuser"
import { GuPDTInvite } from "./database/entity/gupdt-invite"
import { GuPDTRacer } from "./database/entity/gupdt-racer"
import { GuPDTRacetrack } from "./database/entity/gupdt-racetrack"
import { VisitorHelper } from "./schoolsystem/helpers/visitor-helper"
import { HelpCommandGroup } from "./helpsystem/help-command-group"
import { BattleCommandGroup6D } from "./battlesystem-6d/battle-command-group"
import { BattleMain6D } from "./battlesystem-6d/battle-main"
import { WeatherMain } from "./weathersystem/weather-main"
import { GuPDTSchool } from "./database/entity/gupdt-school"
import { SchoolCommandGroup } from "./schoolsystem/school-command-group"

const adapter = new Adapter({ token })
const bot = new Bot({
    adapter,
    commands: [BattleCommandGroup, BattleCommandGroup6D, DebugCommandGroup, WalletCommandGroup, SchoolCommandGroup, RacingCommandGroup, HelpCommandGroup],
    services: [RacingMain, BattleMain, BattleMain6D, WeatherMain] 
})

async function main() {
  await bot.start()
  console.log("Bot is running!")
  setTimeout(() => {
    console.log("Ready - Intializing")
    
    bot.client.guilds.cache.forEach(guild => {
      guild.channels.cache.forEach(channel => {
        if (channel.parent != null && channel.type === "text") {
          let textChannel = channel as TextChannel
          if (textChannel.topic != null && textChannel.topic.includes("[rp-b]")) {
            bot.manager.getService(BattleMain).channels.push(channel.id);
            bot.manager.getService(BattleMain6D).channels.push(channel.id);
          }
          if (textChannel.topic != null && textChannel.topic.includes("[rp-m]")) {
            
          }
          if (textChannel.topic != null && textChannel.topic.includes("[rp-weather]")) {
            console.log("Initialize Weather Tick")
            bot.manager.getService(WeatherMain).channels.push(textChannel);
          }
        }
      })
    })
    bot.manager.getService(WeatherMain).startChannelTick();
  }, 5000);


  setInterval(() => {
    console.log("Clearing invites...")
    const visitorHelper = new VisitorHelper();
    visitorHelper.clearInvites(bot);
  }, 3600000)

  const connection: Connection = await createConnection({
    type: "mysql",
    host: ormConfig.host,
    port: 3306,
    username: ormConfig.username,
    password: ormConfig.password,
    database: ormConfig.database,
    entities: [GuPDTUser, GuPDTTank, GuPDTWallet, GuPDTEvent, GuPDTEventStage, GuPDTEventUser, GuPDTInvite, GuPDTRacer, GuPDTRacetrack, GuPDTSchool],
    synchronize: true,
  });
}

main()
