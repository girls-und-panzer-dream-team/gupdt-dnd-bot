import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";

export const HelpCommand = new Command()
  .match(matchPrefixes(""))
  .use(async (context) => {
    const { manager, message } = context;
    message.channel.send("**This Bot is designed by Natalie#1702 for use with the Girls und Panzer Dream Team Roleplay**\n"+
      "The Bot offers functions for several areas:\n" +
      " - **Tank Battles __CLASSIC__:** Sensha-Dó is the heart of Girls und Panzer, naturally you'll want to have Tankbattles channels with [rp-b] in their description will let you fight there - to learn more use:\n`!battle help`\n" +
      " - **Tank Battles __ADVANCED__:** Sensha-Dó advanced matches have additional features and rewards - channels with [rp-b] in their description will let you fight there - to learn more use:\n`?battle help`\n" +
      " - **Racing:** Originally meant for racing Tanks channels with [rp-track] allow you to go for a timed race - to learn more use: \n`!racing help`\n" +
      " - **Wallet/Money:** Your wallet allows you to pay other Characters NPCs in RP or later on repair tanks buy things - to learn more use: \n`!wallet help`\n" +
      "\n" +
      "Misc Commands:\n" +
      " - **Invite to school carrier:** You can temporarily invite someone to your School Carrier as visitor so they can RP with you there for 48h from #port use:\n" +
      "`!school invite @[username]`\n\n" +
      "Please leave feedback or bug reports in #gupdt-dnd-bot-feedback")
  });
