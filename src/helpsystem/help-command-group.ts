import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { HelpCommand } from "./help-command"


export const HelpCommandGroup = new CommandGroup()
  .match(matchPrefixes("!help"))
  .setCommands(HelpCommand)